--[[

    Client Identifier library.
    Version 27/01/2019.

    Description:
    A Lua library to identify client(s) from UI module.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Global instance.
ClientID =
{
    Debug = {}
    , Game = {}
    , UI = {}
    , SyncValue =
    {
        Get = {}
        , GetAll = {}
        , Verify = {}
        , CurrentTime = {}
    }
    , Enum = {}
};


-- Client ID SyncValue variable name.
ClientID.SyncValue.Get.NAME         = "ClientIdentifier.Get";
ClientID.SyncValue.GetAll.NAME      = "ClientIdentifier.GetAll";
ClientID.SyncValue.Verify.NAME      = "ClientIdentifier.Verify";
ClientID.SyncValue.CurrentTime.NAME = "ClientIdentifier.CurrentTime";


-- Client ID signals.
ClientID.Enum.SIGNAL = 
{
    OK              = 2000000000
    , GET           = 1547109751
    , GET_ALL       = 1547109750
    , VERIFY        = 1547109749
    , VERIFIED      = 1548485347
    , NOT_VERIFIED  = 1548485348
};


-- Prints debug msg.
function ClientID.Debug:print( msg )
    if self.ENABLED
    then
        print( msg );
    end
end


print( "ClientIdentifier.Shared is loaded!" );

