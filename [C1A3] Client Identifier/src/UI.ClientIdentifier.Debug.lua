--[[

    Client Identifier library.
    Version 10/01/2019.

    Description:
    The debugger for Client Identifier.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Local variables.
local module = ClientID.UI;
local debug = ClientID.Debug;
local syncvalue = ClientID.SyncValue;
local enum = ClientID.Enum;
local doPrintMyInfo = false;
local doPrintAllInfo = false;
local syncvar_get = UI.SyncValue.Create( syncvalue.Get.NAME );
local syncvar_get_all = UI.SyncValue.Create( syncvalue.GetAll.NAME );


function debug:OnChat( msg )
    if string.find( msg, "myinfo" )
    then
        print( "Sending GET signal..." );
        UI.Signal( enum.SIGNAL.GET );
        doPrintMyInfo = true;
    elseif string.find( msg, "uiinfo" )
    then
        print( "My UI ID: " .. tostring( module.GetID() ) );
        print( "CLIENTID_GET: " .. tostring( syncvar_get.value ) );
    elseif string.find( msg, "allinfo" )
    then
        print( "Sending GET_ALL signal..." );
        UI.Signal( enum.SIGNAL.GET_ALL );
        doPrintAllInfo = true;
	end
end


function debug:OnSync()
    if doPrintMyInfo
    then
        doPrintMyInfo = false;
        print( "My UI ID: " .. tostring( module:GetID() ) );
        print( "My Game ID: " .. tostring( syncvar_get.value ) );
    end
end



function debug:OnSync2()
    if doPrintAllInfo
    then
        doPrintAllInfo = false;
        print( "All Player ID(s):" );
        for k,v in ipairs( explode("\n", tostring(syncvar_get_all.value)) )
        do
            print( k .. ":" .. v );
        end
    end
end


function UI.Event:OnChat( msg )
    debug:OnChat( msg );
end
function syncvar_get:OnSync()
    debug:OnSync();
end
function syncvar_get_all:OnSync()
    debug:OnSync2();
end


--[[ from this link: http://lua-users.org/wiki/SplitJoin ]]
-- explode(seperator, string)
function explode(d,p)
    local t, ll
    t={}
    ll=0
    if(#p == 1) then
        return {p}
    end
    while true do
        l = string.find(p, d, ll, true) -- find the next d in the string
        if l ~= nil then -- if "not not" found then..
            table.insert(t, string.sub(p,ll,l-1)) -- Save it in our array.
            ll = l + 1 -- save just after where we found it for searching next time.
        else
            table.insert(t, string.sub(p,ll)) -- Save what's left in our array.
            break -- Break at end, as it should be, according to the lua manual.
        end
    end
    return t
end

