--[[

    ChatSound2 plugin.
    Version 25/12/2018.

    Description:
    The configuration file for ChatSound2.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


--[[
    [ID]
    Buat dahulu SFX block di map nya,
    lalu tambahkan nilai baru di dalam variabel "chatConfigs" dengan format:

        {
            "nama_suara",
            { x = 0 , y = 0 , z = 0 }
        }

    Keterangan:
    - nama suara harus berupa huruf kecil.
    - x, y, dan z merupakan koordinat dari SFX block.

    [EN]
    First creates your SFX block in your map,
    then add a new value inside of variable "chatConfigs" with format:
    
        {
            "sound_name",
            { x = 0 , y = 0 , z = 0 }
        }

    Info:
    - sound name must be lowercase.
    - x, y, and z are the coordinate of SFX block.
]]
chatConfigs = 
{
    {
        "helicopter",
        { x=3, y=3, z=-94 }
    },

    {
        "beep",
        { x=3, y=2, z=-94 } 
    },

    {
        "scream1",
        { x=3, y=1, z=-94 }
    },

    {
        "scream2",
        { x=3, y=0, z=-94 }
    },

    {
        "wolf",
        { x=3, y=-1, z=-94 }
    },

    {
        "dog",
        { x=3, y=-2, z=-94 }
    },

    {
        "cat",
        { x=3, y=-3, z=-94 }
    },

    {
        "bird",
        { x=3, y=-4, z=-94 }
    }
};


print("ChatSound.Configs is loaded!");

