--[[

    ChatSound2 plugin.
    Version 25/12/2018.

    Description:
    A Lua fun plugin to introduces chat-to-sound feature on your map.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


local SIGNAL_UNIQUE_ID = 5000;
function Game.Rule:OnPlayerSignal( player, signal )
    for k,v in ipairs(chatConfigs)
    do
        if signal == SIGNAL_UNIQUE_ID + k
        then
            if v ~= nil
            then
                local object = nil;
                object = Game.EntityBlock.Create( v[2] );

                if object ~= nil
                then
                    --print( k .. ":" .. v[1] );
                    object:Event( { action="signal" , value = false }, 0 );
                    object:Event( { action="signal" , value = true }, 0 );
                    object:Event( { action="signal" , value = false }, 0 );
                    return;
                end
            end
        end
    end

    print( string.format("[Game] InvalidCSSignal(%s)" , tostring(signal)) );
end


print("Game.ChatSound.Main is loaded!");

