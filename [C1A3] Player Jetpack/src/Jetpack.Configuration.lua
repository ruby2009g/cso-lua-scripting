--[[

    Player Jetpack plugin.
    Version 01/03/2019.

    Description:
    A Lua plugin to brings a new feature to the players, a functional jetpack.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- #Debug settings.
-- Debug mode.
Jetpack.Debug.Enabled = true;


-- #Game settings.
-- Default gives the jetpack to player.
Jetpack.Game.DEFAULT_GIVE = true;


-- Default turns on the jetpack.
Jetpack.Game.DEFAULT_ON = true;


-- Maximum fuel value.
Jetpack.FUEL_MAX = 100;


-- Starting fuel value.
Jetpack.Game.FUEL_INIT = Jetpack.FUEL_MAX;


-- Fuel depletion rate each 0.1 second.
Jetpack.Game.FUEL_DEPLETION_RATE = 0.25;


-- Fuel regeneration rate each 0.1 second.
Jetpack.Game.FUEL_REGEN_RATE = 0.5;


-- Max forward, backward, left, and right movement speed.
Jetpack.Game.MOVE_SPEED_MAX = 575;


-- Max upward movement speed.
Jetpack.Game.MOVE_UPWARD_SPEED_MAX = 200;


-- Forward, backward, left, and right acceleration rate each 0.1 second.
Jetpack.Game.MOVE_SPEED_RATE = 50;


-- Upward acceleration rate each 0.1 second.
Jetpack.Game.MOVE_UPWARD_SPEED_RATE = 100;


-- Because player speed is updated each 0.1 second, its still too late to matching the real upward speed.
-- This is extra speed for ALMOST synchronized upward speed.
Jetpack.Game.MOVE_UPWARD_SPEED_EXTRA = 80;


-- Stable up/down movement speed for landing.
Jetpack.Game.LANDING_SPEED_STABLE = 350;


-- #UI settings.
-- Fuel tooltip text configurations.
Jetpack.UI.Text.Tooltip.Config =
{
    font        = 'small'
    , align     = 'left'
    , width     = 260
    , height    = 100
    , r         = 255
    , g         = 160
    , b         = 0
    , text      = 'Jetpack Tooltip :\n\n'
                   .. 'T         : Turn on/off.\n'
                   .. 'Space   : Move upward.\n'
                   .. 'W,S,A,D : Movements while flying.\n'
                   .. 'SHIFT    : Landing.'
};


-- Fuel label text configurations.
Jetpack.UI.Text.Fuel.Label.Config =
{
    font        = 'small'
    , align     = 'left'
    , width     = 105
    , height    = 15
    , r         = 255
    , g         = 160
    , b         = 0
    , text      = 'Jetpack Fuel: '
};


-- Fuel percentage text configurations.
Jetpack.UI.Text.Fuel.Percentage.Config =
{
    font        = 'medium'
    , align     = 'left'
    , width     = 120
    , height    = 30
    , text      = '%.0f%%'
};


-- Status label text configurations.
Jetpack.UI.Text.Status.Label.Config =
{
    font        = 'small'
    , align     = 'left'
    , width     = 65
    , height    = 15
    , r         = 255
    , g         = 160
    , b         = 0
    , text      = 'Status: '
};


-- Status on/off text configurations.
Jetpack.UI.Text.Status.OnOff.Config =
{
    font        = 'small'
    , align     = 'left'
    , width     = 45
    , height    = 15

    , On =
    {
        text     = 'On'
        , r      = 0
        , g      = 255
        , b      = 0
    }
    
    , Off =
    {
        text    = 'Off'
        , r     = 128
        , g     = 128
        , b     = 128
    }
};


-- Fuel bar configurations.
Jetpack.UI.Box.Fuel.Config =
{
    amount      = 10
    , width     = 5
    , height    = 40

    , Low =
    {
        r = 255
        , g = 0
        , b = 0
        , a = 255
    }

    , Medium =
    {
        r = 255
        , g = 255
        , b = 0
        , a = 255
    }

    , High =
    {
        r = 0
        , g = 255
        , b = 0
        , a = 255
    }
};


print( "Jetpack.Configuration is loaded!" );

