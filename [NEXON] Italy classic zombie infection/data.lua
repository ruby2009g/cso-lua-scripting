-- 좀비와 인간 플레이어의 수치 테이블
param = {
	human = {
		team = Game.TEAM.CT,
		model = Game.MODEL.DEFAULT,
		hp = 100,
		armor = 0,
		flinch = 1.0,
		knockback = 1.0
	},
	zombie = {
		team = Game.TEAM.TR,
		model = Game.MODEL.NORMAL_ZOMBIE_HOST,
		hp = 1000,
		armor = 500,
		flinch = 0.8,
		knockback = 1.2
	}
}

-- 타이머 클래스
Timer = { valid = false, timer = 0}
function Timer:Init()
	self.valid = false
	self.timer = 0
end

function Timer:IsValid()
	return self.valid
end

function Timer:Start(duration)
	self.valid = true
	self.timer = Game.GetTime() + duration
end

function Timer:IsElapsed()
	if self.valid == false then return false end
	
	return self.timer < Game.GetTime()
end

function Timer:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end