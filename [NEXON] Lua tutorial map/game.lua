--[[
1. 플레이어의 수치 변경
	- 버튼으로 플레이어의 수치를 변경하기
]]
button1 = Game.EntityBlock.Create({x=-42, y=-24, z=2})
function button1:OnUse(player)
	if self.onoff == true then
		player.health = 10
		player.armor = 10
	else
		player.health = 100
		player.armor = 100
	end
end


--[[
2. 채팅 이벤트
	- 플레이어가 보낸 신호로 문 열기
]]
SIGNAL = { TRIGGER1 = 1 }
trigger2 = Game.EntityBlock.Create({x=-42, y=-31, z=2})
door2 = Game.EntityBlock.Create({x=-42, y=-35, z=1})
function Game.Rule:OnPlayerSignal(player, signal)
	-- 시작
	if signal == SIGNAL.TRIGGER1 then
		trigger2:Event({action="use"}, 0)
	end
end

--[[
3. UI 표시
	- 플레이어에게 UI 요소를 생성하고 변경하기
]]
sync1 = Game.SyncValue.Create("sync1")
button3_1 = Game.EntityBlock.Create({x=-45, y=-40, z=1})
function button3_1:OnUse(player)
	sync1.value = "1번"
end
button3_2 = Game.EntityBlock.Create({x=-45, y=-39, z=1})
function button3_2:OnUse(player)
	sync1.value = "2번"
end
button3_3 = Game.EntityBlock.Create({x=-45, y=-38, z=1})
function button3_3:OnUse(player)
	sync1.value = "3번"
end

--[[
4. 특수한 장치 블록
	- 어떤 장치 블록은 특수한 이벤트를 받기도 합니다
]]
textboard = Game.EntityBlock.Create({x=-42, y=-48, z=2})
trigger4_1 = Game.EntityBlock.Create({x=-42, y=-47, z=1})
function trigger4_1:OnUse(player)
	textboard:Event({action="text", value="Hello"})
end
trigger4_2 = Game.EntityBlock.Create({x=-43, y=-47, z=1})
function trigger4_2:OnUse(player)
	textboard:Event({action="text", value="World"})
end

--[[
5. 스크립트를 이용한 승리
	- 코인과 스크립트를 이용해 승리 조건을 만족해보세요
]]
switch5 = Game.EntityBlock.Create({x=-52, y=-46, z=2})
function switch5:OnUse(player)
	if player.coin >= 2000 then
		player:Win()
	end
end