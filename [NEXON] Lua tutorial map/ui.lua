

-- 2번 방 채팅 이벤트
SIGNAL = { TRIGGER1 = 1 }
function UI.Event:OnChat(msg)
	if string.find(msg, "start") then
		UI.Signal(SIGNAL.TRIGGER1)
	end
end

-- 3번 방 UI
labelBG = UI.Box.Create()
labelBG:Set({x=50, y=50, width=100, height=100, r=50, g=50, b=50, a=200})
labelBG:Hide()
label = UI.Text.Create()
label:Set({font="large", align="center", x=50, y=50, width=100, height=100, r=255, g=255, b=255})
label:Hide()

sync1 = UI.SyncValue.Create("sync1")
function sync1:OnSync()
	labelBG:Show()
	label:Show()
	label:Set({text = self.value})
end