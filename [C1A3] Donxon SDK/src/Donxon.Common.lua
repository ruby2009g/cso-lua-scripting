------------
-- Donxon Global Properties.
--
-- Properties that are accessible at a global level.
--
-- @global Properties

local g_emptyFunc = function()end

--- Debug mode.
--
-- @tfield bool _DEBUG Set to `true` to enable debug mode.
_DEBUG = true;

--- @section end


------------
-- Donxon Global Functions.
--
-- Functions that are accessible at a global level.
--
-- @global Functions


--- Checks whether `Game` module is loaded.
--
-- @treturn bool Returns true if Game module is loaded.
function IsGameModule ()
    return Game ~= nil and type(Game) == "table";
end


--- Checks whether `UI` module is loaded.
--
-- @treturn bool Returns true if UI module is loaded.
function IsUIModule ()
    return UI ~= nil and type(UI) == "table";
end


--- Prints a debug message into console.
--
-- **Note:** Is active when debug mode (`_DEBUG`) is enabled.
--
-- @param ... Receives any number of arguments and prints their values.
function debug_print (...)
    if not _DEBUG then return; end
    print( "[D]" , (IsGameModule() and "[G]" or "[U]"), ... );
end


--- Math Library
--
-- @section math

--- Clamps a number between a minimum and maximum value.
--
-- @tparam number min The minimum value, this function will never return a number less than this.
-- @tparam number max The maximum value, this function will never return a number greater than this.
-- @tparam number value The number to clamp.
-- @treturn number The clamped value.
function math.clamp (min, max, value)
    return math.min( max, math.max( min , value ) );
end


--- Rounds the number.
--
-- @tparam number num The value to round.
-- @tparam[opt=0] number numDecimalPlaces The number of decimal place(s).
-- @treturn number The rounded number.
function math.round (num, numDecimalPlaces)
    return tonumber(string.format("%." .. (numDecimalPlaces or 0) .. "f", num));
end


--- String Library
--
-- @section string

--- Splits a string into a `table`.
--
-- Taken from this link: http://lua-users.org/wiki/SplitJoin
--
-- @string source The string to explode.
-- @string delimiter The string delimiter.
-- @treturn table The result.
function string.explode (source , delimiter)
    local t, l, ll
    t={}
    ll=0
    if(#source == 1) then
        return {source}
    end
    while true do
        l = string.find(source, delimiter, ll, true) -- find the next d in the string
        if l ~= nil then -- if "not not" found then..
            table.insert(t, string.sub(source,ll,l-1)) -- Save it in our array.
            ll = l + 1 -- save just after where we found it for searching next time.
        else
            table.insert(t, string.sub(source,ll)) -- Save what's left in our array.
            break -- Break at end, as it should be, according to the lua manual.
        end
    end
    return t
end


--- Table Library
--
-- @section table

--- Sets the `table` access to read-only using `metatable`.
--
-- @tparam table table The table to set.
-- @string[opt] msg The error message when invokes table editing.
-- @treturn table The result.
function table.readonly (table, msg)
    msg = msg or "read-only table.";
    return setmetatable({}, {
        __index = table,
        __newindex = function()
            error(msg, 2);
        end,
        __metatable = false
    });
end


--- Extends a `table` using `metatable`.
--
-- @tparam table base The base index table.
-- @tparam table derived The derived table.
-- @bool[opt] hasWriteOnlyMember Base table has write-only members.
-- @treturn table The extended derived table.
function table.extend (base, derived, hasWriteOnlyMember)
    assert( base ~= derived , "Can't extend with same table address." );

    derived._base = base;
    local mt = getmetatable( derived );
    mt = (type(mt) == "table") and mt or {};
    mt.__hasWriteOnlyMember = hasWriteOnlyMember;
    mt.__index_old = mt.__index;
    mt.__index = function (self, key)
        local result = nil;
        -- Use derived metamethod.
        if mt.__index_old then
            if type(mt.__index_old) == "function" then
                result = mt.__index_old(self, key);
            else
                result = mt.__index_old[key];
            end
        end

        -- Use base table.
        if result == nil then
            local base = rawget(self, "_base");
            result = base[key];

            if type( result ) == "function" then
                result = function (this, ...)
                    if this == self then this = base; end
                    return base[key](this, ...);
                end;
            end
        end

        return result;
    end;
    mt.__newindex_old = mt.__newindex;
    mt.__newindex = function (self, key, value)
        -- Use derived metamethod.
        if mt.__newindex_old then
            if type(mt.__newindex_old) == "function" then
                mt.__newindex_old(self, key, value);
            else
                mt.__newindex_old[key] = value;
            end
            return;
        end

        -- Use base table.
        local base = self._base;
        local tipe = type(value);
        if mt.__hasWriteOnlyMember and tipe ~= "number" and tipe ~= "string"
        then
            base[key] = value;
            return;

        else
            if base[key] ~= nil
            then
                base[key] = value;
                return;
            end
        end

        -- Otherwise, usual value assignment.
        rawset( self, key, value );
    end;

    return setmetatable(derived, mt);
end


--- Merges two tables.
--
-- Taken from this link: https://stackoverflow.com/a/29133654.
--
-- @tparam table target The target table to merge with.
-- @tparam table source The source table.
-- @tparam[opt=false] bool deep Do recursive merge.
-- @treturn table The merget target table.
function table.merge (target, source, deep)
    if deep == nil then deep = false; end
    assert( type( target ) == 'table', "`target` expected table, got " .. type( target ) .. " instead." );
    assert( type( source ) == 'table', "`source` expected table, got " .. type( source ) .. " instead." );

    for k,v in pairs( source ) do
        if deep and type(v) == 'table' and type( target[k] or false ) == 'table' then
            table.merge( target[k], v );
        else
            target[k] = v;
        end
    end

    return target;
end


--- Clones a table.
--
-- Taken from this link: https://stackoverflow.com/a/16077650.
--
-- @tparam table source The source table to clone.
-- @tparam[opt=true] bool deep Do recursive copy.
-- @tparam[opt=nil] table seen Last seen sub-table.
-- @return table The cloned table.
function table.clone ( source, deep, seen )
    seen = seen or {};
    if source == nil then return nil; end
    if seen[ source ] then return seen[ source ]; end

    local result;
    if type( source ) == 'table' then
        result = {};

        if deep then
            seen[ source ] = result;
            for k, v in next, source, nil do
                result[ table.clone(k, seen) ] = table.clone( v, seen );
            end
            setmetatable( result, table.clone( getmetatable( source ), seen ) );

        else
            for k, v in pairs( source ) do
                result[ k ] = v;
            end
        end
    else -- number, string, boolean, etc.
        result = source;
    end

    return result;
end

--- @section end


------------
-- Donxon Global Classes.
--
-- Classes that are accessible at a global level.
--
-- @global Classes


--- String Buffer class.
--
-- Fast string buffer with "Tower of Hanoi" strategy algorithm.
--
-- @tfield StringBuffer StringBuffer


--- Hook Management class.
--
-- Creating multiple hooks become more easier.
--
-- @tfield Hook Hook

--- @section end


------------
-- String Buffer class.
--
-- Fast string buffer with "Tower of Hanoi" strategy algorithm.
--
-- Based on [snippet code](http://www.lua.org/pil/11.6.html) in "Programming in Lua (first edition)" e-book.
--
-- @classmod StringBuffer
local StringBuffer = {};


--- Methods
-- @section method

--- Checks whether the table is compatible with string buffer class.
--
-- @local
-- @tparam table v The table to check.
-- @bool[opt=true] checkMembers Asserts members data type.
-- @treturn bool Return true if table is compatible.
function StringBuffer:AssertTable ( v , checkMembers )
    assert( type( v ) == "table", "`v` expected table, got " .. type( v ) .. " instead." );
    checkMembers = (checkMembers == nil) and true or checkMembers;
    if checkMembers then
        assert( type( v.content ) == "table", "property `content` expected table, got " .. type( v.content ) .. " instead." );
    end

    return true;
end


--- Constructs a string buffer.
--
-- @tparam[opt=""] string init The init string.
-- @treturn StringBuffer A new string buffer.
function StringBuffer:Create ( init )
    -- Class properties.
    local o =
    {
        content = { init or "" }
    };

    setmetatable( o, StringBuffer );

    -- Checks again.
    self:AssertTable( o );

    --debug_print( "[SB]", "(C)" );
    return o;
end


--- Returns the buffered string.
--
-- @treturn string The buffered string.
function StringBuffer:ToString ()
    return table.concat( self.content );
end


--- Metamethods
-- @section metamethod

--- Base class.
-- @tfield StringBuffer __index
StringBuffer.__index = StringBuffer;


--- Returns the buffered string.
-- @tfield StringBuffer.ToString __tostring
StringBuffer.__tostring = StringBuffer.ToString;


--- Returns a concatenated form of this string buffer.
--
-- @tparam string val The string to concat.
function StringBuffer:__concat ( val )
    local stack = self.content;
    table.insert( stack, val );   -- push 's' into the the stack
    for i = #stack - 1, 1, -1 do
        if string.len( stack[i] ) > string.len( stack[i+1] ) then
            break;
        end
        stack[i] = stack[i] .. table.remove( stack );
    end
    --debug_print( "[SB]", "(..)", val );
    return self;
end


--- Returns the length of this string buffer.
--
-- @treturn number The length of this string buffer.
function StringBuffer:__len ()
    return string.len( self:ToString() );
end


--- Event Callbacks
-- @section callback

--- @section end


------------
-- Hook Management class.
--
-- Creating multiple hooks become more easier.
--
-- @classmod Hook
Hook = {};


--[[--------------------------------------
    [HOOK] Enumerations.
----------------------------------------]]

--- Hook function return codes.
Hook.RETURN = {
    CONTINUE = function()end,  -- Returned when a hook function has not handled the call.
    HANDLED  = function()end,  -- Returned when a hook function has handled the call.
}; Hook.RETURN = table.readonly( Hook.RETURN );


--[[--------------------------------------
    [HOOK] Data structures.
----------------------------------------]]

--- The hook doubly linked list data structure.
--
-- @struct .hookLink
-- @tfield .hookLink _previous The previous link.
-- @tfield function _current The hook function.
-- @tfield .hookLink _next The next link.


--- The hook index page data structure.
--
-- @struct .hookIndexPage
-- @tfield table _container The hook container parent table.
-- @tfield table _base The base hook class.
-- @tfield string _name The hook name.
-- @tfield .hookLink _first The first hook.
-- @tfield .hookLink _last The last hook.


--- The hook container data structure.
--
-- @struct .hookContainer
-- @tfield table _base The base hook class.
-- @tfield {[string]=.hookIndexPage,...} _hooks The hook index pages.
-- @tfield table _baseMethodTable The base table for non-hook methods redirection.
-- @tfield[opt] string|string[]|function _hookNameComparator The comparator of hook method name (Lua Regex | Hook name list | Comparator function). (_default_ "[Oo]n%u.*")


--- Methods
-- @section method

--- Creates a hook container.
--
-- @tparam table base The base hook class.
-- @tparam table[opt] baseMethodTable The base table for methods redirection.
-- @tparam[opt] ?string|string[]|function hookNameComparator The comparator of hook method name (Lua Regex | Hook name list | Comparator function). (_default_ "[Oo]n%u.*")
-- @treturn .hookContainer A hook container.
function Hook:Create ( base, baseMethodTable, hookNameComparator )
    hookNameComparator = hookNameComparator or "[Oo]n%u.*";
    assert( type(hookNameComparator) == "string" or type(hookNameComparator) == "table" or type(hookNameComparator) == "function", "`hookNameComparator` expected string|table|function, got " .. type(hookNameComparator) .. " instead." );
    return setmetatable({
            _base = base,
            _hooks = {},
            _baseMethodTable = baseMethodTable,
            _hookNameComparator = hookNameComparator
        }, {
        __index = function (self, key)
            local result = rawget( self, "_hooks" )[key];
            if not result then
                result = rawget( self, "_base" )[key];

                local base = rawget( self, "_baseMethodTable" );
                if base and type( result ) == "function" then
                    return function (this, ...)
                        if this == self then this = base; end
                        return base[key](this, ...);
                    end;
                end
            end
            return result;
        end
        , __newindex = function (self, key, value)
            local isHook = (type(value) == "function");

            -- Checks with hook name comparator.
            local comparator = self._hookNameComparator;
            if isHook and comparator then
                if type(comparator) == "string" then
                    isHook = key:find( comparator );

                elseif type(comparator) == "table" then
                    isHook = false;
                    for _,v in pairs( comparator ) do
                        if v == key then
                            isHook = true;
                            break;
                        end
                    end

                elseif type(comparator) == "function" then
                    isHook = comparator();
                end
            end


            -- Is a hook method.
            if isHook then
                local hooks = self._hooks;
                if type(hooks[key]) ~= "table" then
                    hooks[key] = setmetatable( { _container = self, _base = self._base, _name = key }, {
                        __call = function (...)
                            local varags = {...};
                            if varags[1] == hooks[key] then table.remove( varags, 1 ); end
                            return self._base[key]( table.unpack(varags) );
                        end });
                    local func = Hook:Link( value );
                    hooks[key]._first = func;
                    hooks[key]._last = func;
                    self._base[key] = func;
                    debug_print( "[H]", "(C)", key);
                else
                    local temp = Hook:Link( value, hooks[key]._last );
                    hooks[key]._last._next = temp;
                    rawset( hooks[key], "_last", temp );
                end
                debug_print( "[H]", "(L)", key);

            -- A regular value assigning.
            else
                self._base[key] = value;
            end
        end
    });
end


--- Creates a doubly linked list of a hook.
--
-- @local
-- @func func The hook function to link.
-- @tparam[opt] table prev The previous link.
-- @tparam[opt] table nxt The next link.
-- @treturn .hookLink A linked hook.
function Hook:Link ( func, prev, nxt )
    assert( type(func) == "function", "`func` expected function, got " .. type(func) .. " instead." );
    assert( prev == nil or type(prev) == "table", "`prev` expected table, got " .. type(prev) .. " instead." );
    assert( nxt == nil or type(nxt) == "table", "`nxt` expected table, got " .. type(nxt) .. " instead." );

    local o = {_previous = prev, _current = func, _next = nxt};
    return setmetatable(o, {
        __call = function ( ... )
            local varags = {...};
            if varags[1] == o then table.remove( varags, 1 ); end
            local nextCall = o._next or g_emptyFunc;
            if type(o._current) ~= "function" then
                debug_print( "[H]", "(L)", "Dangling pointer.");
                if type(o._next) == "table" then
                    o._next._previous = o._previous;
                end
                if type(o._previous) == "table" then
                    o._previous._next = o._next;
                end
                return nextCall( table.unpack(varags) );
            end

            local callReturns = table.pack( o._current( table.unpack(varags) ) );
            local returnCode = callReturns[1];
            if returnCode == Hook.RETURN.CONTINUE then table.remove( callReturns, 1 ); end
            if callReturns.n == 0 then
                callReturns = table.pack( nextCall( table.unpack(varags) ) );
                returnCode = callReturns[1];
            end

            if callReturns.n > 0 then
                if returnCode == Hook.RETURN.HANDLED then return; end
                if #callReturns > 0 then return table.unpack( callReturns ); end
                return nil;
            end
        end
    });
end


--- Removes a hook.
--
-- @tparam table hooks The hook name.
-- @tparam ?function|table func The hook function reference to remove.
-- @treturn boolean Returns `true` if succeed. Otherwise, returns `false`.
function Hook:Remove ( hooks, func )
    assert( type(hooks) == "table", "`hooks` expected table, got " .. type(hooks) .. " instead." );
    func = (type(func) == "table") and func._first._current or func;
    assert( type(func) == "function", "`func` expected function, got " .. type(func) .. " instead." );

    local key = hooks._name;
    local first, last = hooks._first, hooks._last;
    local o = last;
    repeat
        if o._current == func then
            if o == last then
                rawset( hooks, "_last", o._previous );
            end
            if o == first then
                rawset( hooks, "_first", o._next );
                hooks._base[key] = o._next or g_emptyFunc;
            end
            if type(o._next) == "table" then
                o._next._previous = o._previous;
            end
            if type(o._previous) == "table" then
                o._previous._next = o._next;
            end
            o._current = nil;
            debug_print( "[H]", "(R)", "{GOOD}", key );
            return true;
        end
        o = o._previous;
    until o == nil;

    debug_print( "[H]", "(R)", "{BAD}", key );
    return false;
end


--- Removes all hooks.
--
-- @tparam table hooks The hook name.
-- @treturn boolean Returns `true` if succeed.
function Hook:RemoveAll ( hooks )
    assert( type(hooks) == "table", "`hooks` expected table, got " .. type(hooks) .. " instead." );

    local key = hooks._name;
    hooks._base[key] = g_emptyFunc;
    rawset( hooks._container, "_hooks", {} );
    debug_print( "[H]", "(RA)", key );
    return true;
end


--- Metamethods
-- @section metamethod


--- Event Callbacks
-- @section callback

--- @section end


------------
-- Donxon Common module.
--
-- This module contains the functionality for scripts running on both the client and server.
--
-- It can be used in script files registered in both "game" and "ui" array of `project.json`.
--
-- @module Common


--[[=========================================================
--  [COMMON] Pre-loading.
=========================================================--]]

--- Stores original `Common` table for later usages.
-- @local
-- @tfield Common baseCommon
local baseCommon = Common;


--[[--------------------------------------
    [COMMON] Common module.
----------------------------------------]]

--- Common module.
-- @local
-- @tfield Common Common
local Common = {};

--- Maximum supported player slots.
--
-- @tfield[opt=24] number maxPlayer
Common.maxPlayer = 24;

--- Zero vector (0, 0, 0).
--
-- @tfield[opt="x = 0 y = 0 z = 0"] Common.Vector vecZero

--- Empty function.
--
-- An empty function used for internal stuffs.
--
-- @tfield function emptyFunc
Common.emptyFunc = g_emptyFunc;


--[[--------------------------------------
    [COMMON] Enumerations.
----------------------------------------]]

--- Network message destination types.
--
-- @see Game.NetMessage:Begin
Common.MSG = {
    BROADCAST       =   0,  -- **Same as ALL** unreliable to all.
    ONE             =   1,  -- reliable to one (msg_entity).
    ALL             =   2,  -- reliable to all.
    INIT            =   3,  -- **Unsupported** write to the init string.
    PVS             =   4,  -- **Unsupported** Ents in PVS of org.
    PAS             =   5,  -- **Unsupported** Ents in PAS of org.
    PVS_R           =   6,  -- **Unsupported** Reliable to PVS.
    PAS_R           =   7,  -- **Unsupported** Reliable to PAS.
    ONE_UNRELIABLE  =   8,  -- **Same as ONE** Send to one client, got don't put in reliable stream, put in unreliable datagram ( could be dropped ).
    SPEC            =   9,  -- **Unsupported** Sends to all spectator proxies.
}; Common.MSG = table.readonly( Common.MSG );


--- Network message types.
--
-- @see Common.NetMessage:Register
-- @see Game.NetMessage:Begin
Common.NETMSG = {
    SHOWMENU        =   nil,  -- This message displays a "menu" to a player (text on the left side of the screen).
    SCREENFADE      =   nil,  -- This message fades the screen.
    BARTIME         =   nil,  -- This message draws a HUD progress bar which is filled from 0% to 100% for the time Duration seconds. Once the bar is fully filled it will be removed from the screen automatically.
    BARTIME2        =   nil,  -- The message is the same as BarTime, but StartPercent specifies how much of the bar is (already) filled.
};


Common.SIGNAL = {};


--- Available colors.
--
-- @table Common.COLOR
Common.COLOR = {
    YELLOWISH   = nil,
    REDISH      = nil,
    GREENISH    = nil,
};


--[[--------------------------------------
    [COMMON] Classes.
----------------------------------------]]

--- Classes
-- @section classes


--- Color class.
--
-- Shared functions and properties for color manipulations.
--
-- @tfield Common.Color Color


--- 2D Vector class.
--
-- Used for many pathfinding and many other operations
-- that are treated as planar rather than 3d.
--
-- @tfield Common.Vector2D Vector2D


--- 3D Vector class.
--
-- @tfield Common.Vector Vector


--- Network Message Builder class.
--
-- Used to builds network messages
-- that will be sent from server
-- to client(s).
--
-- @tfield Common.NetMessage NetMessage


--- Common Text Menu class.
--
-- Shared functions and properties for text menu classes.
--
-- @tfield Common.TextMenu TextMenu


--- Common ScreenFade class.
--
-- Shared functions and properties for screen fade classes.
--
-- @tfield Common.ScreenFade ScreenFade

--- @section end


------------
-- Color class.
--
-- Shared functions and properties for color manipulations.
--
-- @classmod Common.Color
Common.Color = {};


--- The color table data structure.
-- @struct .color
--
-- @tfield int r Red color composition (Range = 0~255).
-- @tfield int g Green color composition (Range = 0~255).
-- @tfield int b Blue color composition (Range = 0~255).


--- Methods
-- @section method

--- Converts hexadecimal to color table.
--
-- @tparam ?number|string val The hexadecimal value.
-- @treturn .color The color table.
function Common.Color.FromHex( val )
    if type( val ) == "string" then
        -- Removes specific headers.
        -- val = string.gsub( val, "0x", "" );
        val = string.gsub( val, "#", "" );
    end

    val = tonumber( val );
    return {
        r = (val & 0xFF0000) >> 16,
        g = (val & 0xFF00) >> 8,
        b = val & 0xFF,
    };
end


--- Converts color to hexadecimal string.
--
-- @tparam ?int|.color r Red color composition. | A color-compatible table.
-- @tparam[opt] int g Green color composition (Ignored if `r` is table).
-- @tparam[opt] int b Blue color composition (Ignored if `r` is table).
-- @treturn string A hexadecimal string.
function Common.Color.ToHex( r, g, b )
    if type( r ) == "table" then
        r,g,b = r.r, r.b, r.g;
    end

    if math.type( r ) == nil then r = 0; end
    if math.type( g ) == nil then g = 0; end
    if math.type( b ) == nil then b = 0; end

    r = math.clamp( 0, 255, r );
    g = math.clamp( 0, 255, g );
    b = math.clamp( 0, 255, b );

    return string.format( "0x%02X%02X%02X%02X", r,g,b,0 );
end


--- Metamethods
-- @section metamethod


--- Event Callbacks
-- @section callback

--- @section end


------------
-- 2D Vector class.
--
-- Used for many pathfinding and many other operations
-- that are treated as planar rather than 3d.
--
-- @classmod Common.Vector2D
Common.Vector2D = {};
local baseVector2D = Common.Vector2D;

--- Vector x variable.
-- @tfield[opt=0] number x

--- Vector y variable.
-- @tfield[opt=0] number y


--- Methods
-- @section method

--- Checks whether the table is compatible with 2D vector class.
--
-- @local
-- @tparam table v The table to check.
-- @bool[opt=false] checkMembers Asserts members data type.
-- @bool[opt=true] setDefault Set invalid members with default value (Ignored if `checkMembers` is true).
-- @treturn bool Return true if table is compatible.
function Common.Vector2D:AssertTable ( v , checkMembers , setDefault )
    assert( type( v ) == "table", "`v` expected table, got " .. type( v ) .. " instead." );
    setDefault = (setDefault == nil) and true or setDefault;
    if checkMembers then
        assert( math.type( v.x ), "property `x` expected number, got " .. type( v.x ) .. " instead." );
        assert( math.type( v.y ), "property `y` expected number, got " .. type( v.y ) .. " instead." );
    elseif setDefault then
        if not math.type( v.x ) then v.x = 0; end
        if not math.type( v.y ) then v.y = 0; end
    end

    return true;
end


--- Checks whether the value is number type.
--
-- @local
-- @number fl The value to check.
-- @treturn bool Return true if value is number type.
function Common.Vector2D:AssertNumber ( fl )
    assert( math.type( fl ), "`fl` expected number, got " .. type( fl ) .. " instead." );

    return true;
end


--- Constructs a 2D vector.
--
-- @tparam ?number|table X The value of X. | The vector-compatible table.
-- @number[opt] Y The value of Y (Ignored if `X` is table).
-- @treturn Common.Vector2D The new vector.
-- @usage
-- -- You can do this.
-- vec = Common.Vector2D:Create( 1 , 2 )
-- -- or do this.
-- vec = Common.Vector2D:Create( { x = 1 , y = 2 } )
-- -- or do this.
-- vec = Common.Vector2D:Create( "1 2" )
function Common.Vector2D:Create ( X , Y )
    -- Class properties.
    local o =
    {
        x = 0
        , y = 0
    };

    if math.type( X )
    then
        assert( math.type( Y ), "`Y` expected number, got " .. type( Y ) .. " instead." );

        o.x = X;
        o.y = Y;

    elseif type( X ) == "string"
    then
        return self:FromString ( X );

    else
        o = X or o;
        self:AssertTable( o );
    end

    setmetatable( o, baseVector2D );

    -- Checks again.
    self:AssertTable( o , true );

    return o;
end


--- Constructs a 2D vector from a string.
-- Format should be:  x y
--
-- @string stringVector The string vector.
-- @treturn Common.Vector2D The new vector.
function Common.Vector2D:FromString ( stringVector )
    local vec = string.explode( stringVector, ' ' );
    return self:Create( tonumber(vec[1]), tonumber(vec[2]) );
end


--- Returns a clone of this vector.
--
-- @treturn Common.Vector2D The new vector.
function Common.Vector2D:Clone ()
    return self:Create{ x = self.x , y = self.y };
end


--- Gets the length of this vector.
--
-- @treturn number The length.
function Common.Vector2D:Length ()
    local x, y = self.x, self.y;
    return math.sqrt(x*x + y*y);
end


--- Returns the normalized form of this vector.
--
-- @treturn Common.Vector2D The normalized vector.
function Common.Vector2D:Normalize ()
    local flLen = self:Length();

    if ( flLen == 0 )
    then
        return self:Create{ x = 0 , y = 0 };
    else
        flLen = 1 / flLen;
        return self:Create{ x = self.x * flLen , y = self.y * flLen };
    end
end


--- Returns the 3D form of this vector.
--
-- @treturn Common.Vector The new 3D vector.
function Common.Vector2D:Make3D ()
    return Common.Vector:Create( self:Clone() );
end


--- Returns a string representation of this vector.
--
-- @treturn string The string of integers.
function Common.Vector2D:ToString ()
    return string.format( "%.0f %.0f" , self.x , self.y );
end


--- Returns a dot product from 2 vectors.
--
-- @tparam Common.Vector2D v The other vector.
-- @treturn number The dot product.
function Common.Vector2D:DotProduct ( v )
    self:AssertTable( v , true );

    return self.x * v.x + self.y * v.y;
end


--- Metamethods
-- @section metamethod

--- Base class.
-- @tfield Common.Vector2D __index
Common.Vector2D.__index = Common.Vector2D;


--- Returns a string representation of this vector.
-- @tfield Common.Vector2D.ToString __tostring
Common.Vector2D.__tostring = Common.Vector2D.ToString;


--- Returns a negated form of this vector.
--
-- @treturn Common.Vector2D The new vector.
-- @usage
-- local vec = Common.Vector2D:Create( 1 , 2 );
-- vec = -vec;
-- print( vec:ToString() ); -- prints "-1 -2".
function Common.Vector2D:__unm ()
    return self:Create{ x = -self.x , y = -self.y };
end


--- Compares this vector.
--
-- @tparam Common.Vector2D v The other vector.
-- @treturn bool Returns true if equals.
-- @usage
-- local vec = Common.Vector2D:Create();
-- print( vec == Common.vecZero ); -- prints "true".
function Common.Vector2D:__eq ( v )
    self:AssertTable( v , true );

    return self.x == v.x and self.y == v.y;
end


--- Returns a vector from addition of 2 vectors.
--
-- @tparam Common.Vector2D v The other vector.
-- @treturn Common.Vector2D The new vector.
-- @usage
-- local vec = Common.Vector2D:Create( 2 , 3 );
-- vec = vec + {x = 2 , y = 1};
-- print( vec:ToString() ); -- prints "4 4".
function Common.Vector2D:__add ( v )
    self:AssertTable( v );

    local result = self:Clone();
    result.x = result.x + v.x;
    result.y = result.y + v.y;

    return result;
end


--- Returns a vector from subtraction of 2 vectors.
--
-- @tparam Common.Vector2D v The other vector.
-- @treturn Common.Vector2D The new vector.
-- @usage
-- local vec = Common.Vector2D:Create( 2 , 3 );
-- vec = vec - {x = 2 , y = 1};
-- print( vec:ToString() ); -- prints "0 2".
function Common.Vector2D:__sub ( v )
    self:AssertTable( v );

    local result = self:Clone();
    result.x = result.x - v.x;
    result.y = result.y - v.y;

    return result;
end


--- Multiplies this vector with a number value.
--
-- @number fl The modifier.
-- @treturn Common.Vector2D The new vector.
-- @usage
-- local vec = Common.Vector2D:Create( 2 , 3 );
-- vec = vec * 2;
-- print( vec:ToString() ); -- prints "4 6".
function Common.Vector2D:__mul ( fl )
    self:AssertNumber( fl );

    local result = self:Clone();
    result.x = result.x * fl;
    result.y = result.y * fl;

    return result;
end


--- Divides this vector with a number value.
--
-- @number fl The modifier.
-- @treturn Common.Vector2D The new vector.
-- @usage
-- local vec = Common.Vector2D:Create( 4 , 6 );
-- vec = vec / 2;
-- print( vec:ToString() ); -- prints "2 3".
function Common.Vector2D:__div ( fl )
    self:AssertNumber( fl );

    local result = self:Clone();
    result.x = result.x / fl;
    result.y = result.y / fl;

    return result;
end


--- Event Callbacks
-- @section callback

--- @section end


------------
-- 3D Vector class.
--
-- @classmod Common.Vector
Common.Vector = {};
local baseVector3D = Common.Vector;

--- Vector x variable.
-- @tfield[opt=0] number x

--- Vector y variable.
-- @tfield[opt=0] number y

--- Vector z variable.
-- @tfield[opt=0] number z


--- Methods
-- @section method

--- Checks whether the table is compatible with 3D vector class.
--
-- @local
-- @tparam table v The table to check.
-- @bool[opt=false] checkMembers Asserts members data type.
-- @bool[opt=true] setDefault Set invalid members with default value (Ignored if `checkMembers` is true).
-- @treturn bool Return true if table is compatible.
function Common.Vector:AssertTable ( v , checkMembers , setDefault )
    assert( type( v ) == "table", "`v` expected table, got " .. type( v ) .. " instead." );
    setDefault = (setDefault == nil) and true or setDefault;
    if checkMembers then
        assert( math.type( v.x ), "property `x` expected number, got " .. type( v.x ) .. " instead." );
        assert( math.type( v.y ), "property `y` expected number, got " .. type( v.y ) .. " instead." );
        assert( math.type( v.z ), "property `z` expected number, got " .. type( v.z ) .. " instead." );
    elseif setDefault then
        if not math.type( v.x ) then v.x = 0; end
        if not math.type( v.y ) then v.y = 0; end
        if not math.type( v.z ) then v.z = 0; end
    end

    return true;
end


--- Checks whether the value is number type.
--
-- @local
-- @number fl The value to check.
-- @treturn bool Return true if value is number type.
function Common.Vector:AssertNumber ( fl )
    assert( math.type( fl ), "`fl` expected number, got " .. type( fl ) .. " instead." );

    return true;
end


--- Constructs a 3D vector.
--
-- @tparam ?number|table X The value of X. | The vector-compatible table.
-- @number[opt] Y The value of Y (Ignored if `X` is table).
-- @number[opt] Z The value of Z (Ignored if `X` is table).
-- @treturn Common.Vector The new vector.
-- @usage
-- -- You can do this.
-- vec = Common.Vector:Create( 1 , 2 , 3 )
-- -- or do this.
-- vec = Common.Vector:Create( { x = 1 , y = 2 , z = 3 } )
-- -- or do this.
-- vec = Common.Vector:Create( "1 2 3" )
function Common.Vector:Create ( X , Y , Z )
    -- Class properties.
    local o =
    {
        x = 0
        , y = 0
        , z = 0
    };

    if math.type( X )
    then
        assert( math.type( Y ), "`Y` expected number, got " .. type( Y ) .. " instead." );
        assert( math.type( Z ), "`Z` expected number, got " .. type( Z ) .. " instead." );

        o.x = X;
        o.y = Y;
        o.z = Z;

    elseif type( X ) == "string"
    then
        return self:FromString ( X );

    else
        o = X;
        self:AssertTable( o );
    end

    setmetatable( o, baseVector3D );

    -- Checks again.
    self:AssertTable( o , true );

    return o;
end


--- Constructs a 3D vector from a string.
-- Format should be:  x y z
--
-- @string stringVector The string vector.
-- @treturn Common.Vector The new vector.
function Common.Vector:FromString ( stringVector )
    local vec = string.explode( stringVector, ' ' );
    return self:Create( tonumber(vec[1]), tonumber(vec[2]), tonumber(vec[3]) );
end


--- Returns a clone of this vector.
--
-- @treturn Common.Vector The new vector.
function Common.Vector:Clone ()
    return self:Create{ x = self.x , y = self.y , z = self.z };
end


--- Gets the length of this vector.
--
-- @treturn number The length.
function Common.Vector:Length ()
    local x, y, z = self.x, self.y, self.z;
    return math.sqrt(x*x + y*y + z*z);
end


--- Returns the normalized form of this vector.
--
-- @treturn Common.Vector The normalized vector.
function Common.Vector:Normalize ()
    local flLen = self:Length();
    if ( flLen == 0 ) then return self:Create{ x = 0 , y = 0 , z = 1 }; end -- ????
    flLen = 1 / flLen;
    return self:Create{ x = self.x * flLen , y = self.y * flLen , z = self.z * flLen };
end


--- Returns the 2D form of this vector.
--
-- @treturn Common.Vector2D The new 2D vector.
function Common.Vector:Make2D ()
    return Common.Vector2D:Create( self:Clone() );
end


--- Gets the length of this vector in 2D.
--
-- @treturn number The length.
function Common.Vector:Length2D ()
    return self:Make2D():Length();
end


--- Returns a string representation of this vector.
--
-- @treturn string The string of integers.
function Common.Vector:ToString ()
    return string.format( "%.0f %.0f %.0f" , self.x , self.y , self.z );
end


--- Returns a dot product from 2 vectors.
--
-- @tparam Common.Vector v The other vector.
-- @treturn Common.Vector The dot product.
function Common.Vector:DotProduct ( v )
    self:AssertTable( v , true );

    return self.x * v.x + self.y * v.y + self.z * v.z;
end


--- Returns a cross product from 2 vectors.
--
-- @tparam Common.Vector v The other vector.
-- @treturn number The cross product.
function Common.Vector:CrossProduct ( v )
    self:AssertTable( v , true );

    return self:Create( self.y*v.z - self.z*v.y, self.z*v.x - self.x*v.z, self.x*v.y - self.y*v.x );
end


--- Metamethods
-- @section metamethod

--- Base class.
-- @tfield Common.Vector __index
Common.Vector.__index = Common.Vector;


--- Returns a string representation of this vector.
-- @tfield Common.Vector.ToString __tostring
Common.Vector.__tostring = Common.Vector.ToString;


--- Returns a negated form of this vector.
--
-- @treturn Common.Vector The new vector.
-- @usage
-- local vec = Common.Vector:Create( 1 , 2 , 3 );
-- vec = -vec;
-- print( vec:ToString() ); -- prints "-1 -2 -3".
function Common.Vector:__unm ()
    return self:Create{ x = -self.x , y = -self.y , z = -self.z };
end


--- Compares this vector.
--
-- @tparam Common.Vector v The other vector.
-- @treturn bool Returns true if equals.
-- @usage
-- local vec = Common.Vector:Create();
-- print( vec == Common.vecZero ); -- prints "true".
function Common.Vector:__eq ( v )
    self:AssertTable( v , true );

    return self.x == v.x and self.y == v.y and self.z == v.z;
end


--- Returns a vector from addition of 2 vectors.
--
-- @tparam Common.Vector v The other vector.
-- @treturn Common.Vector The new vector.
-- @usage
-- local vec = Common.Vector:Create( 2 , 3 , 4 );
-- vec = vec + {x = 2 , y = 1 , z = 0};
-- print( vec:ToString() ); -- prints "4 4 4".
function Common.Vector:__add ( v )
    self:AssertTable( v );

    local result = self:Clone();
    result.x = result.x + v.x;
    result.y = result.y + v.y;
    result.z = result.z + v.z;

    return result;
end


--- Returns a vector from subtraction of 2 vectors.
--
-- @tparam Common.Vector v The other vector.
-- @treturn Common.Vector The new vector.
-- @usage
-- local vec = Common.Vector:Create( 2 , 3 , 4 );
-- vec = vec - {x = 2 , y = 1 , z = 3};
-- print( vec:ToString() ); -- prints "0 2 1".
function Common.Vector:__sub ( v )
    self:AssertTable( v );

    local result = self:Clone();
    result.x = result.x - v.x;
    result.y = result.y - v.y;
    result.z = result.z - v.z;

    return result;
end


--- Multiplies this vector with a number value.
--
-- @number fl The modifier.
-- @treturn Common.Vector The new vector.
-- @usage
-- local vec = Common.Vector:Create( 2 , 3 , 4 );
-- vec = vec * 2;
-- print( vec:ToString() ); -- prints "4 6 8".
function Common.Vector:__mul ( fl )
    self:AssertNumber( fl );

    local result = self:Clone();
    result.x = result.x * fl;
    result.y = result.y * fl;
    result.z = result.z * fl;

    return result;
end


--- Divides this vector with a number value.
--
-- @number fl The modifier.
-- @treturn Common.Vector The new vector.
-- @usage
-- local vec = Common.Vector:Create( 4 , 6 , 8 );
-- vec = vec / 2;
-- print( vec:ToString() ); -- prints "2 3 4".
function Common.Vector:__div ( fl )
    self:AssertNumber( fl );

    local result = self:Clone();
    result.x = result.x / fl;
    result.y = result.y / fl;
    result.z = result.z / fl;

    return result;
end


--- Event Callbacks
-- @section callback

--- @section end


------------
-- Network Message Builder class.
--
-- Used to builds network messages
-- that will be sent from server
-- to client(s).
--
-- @classmod Common.NetMessage
Common.NetMessage = {};
local baseNetMessage = Common.NetMessage;

--- Name of this network message.
-- @tfield string name

--- The syncvalue of this network message.
-- @local
-- @tfield ?Game.SyncValue|UI.SyncValue syncvalue

--- Group separator.
-- @local
-- @tfield string GRPSEP
Common.NetMessage.GRPSEP = string.char(30);

--- Argument separator.
-- @local
-- @tfield string ARGSEP
Common.NetMessage.ARGSEP = string.char(31);


--- The net message argument property data structure.
-- @struct .netMessageArgumentProperty
--
-- @tfield anything value The argument value.
-- @tfield string format The argument format/datatype.


--- Argument formats.
Common.NetMessage.ARGFORMAT = {
    BYTE    = "byte", -- Unsigned char
    CHAR    = "char", -- Signed char.
    SHORT   = "short", -- Signed short.
    LONG    = "long", -- Signed long.
    ANGLE   = "angle", -- Float in signed char form.
    FLOAT   = "float", -- Float.
    VECTOR  = "vector", -- Vector string.
    STRING  = "string", -- String.
    NUMBER  = "number", -- Lua number.
}; Common.NetMessage.ARGFORMAT = table.readonly( Common.NetMessage.ARGFORMAT );

--- Methods
-- @section method

--- Checks whether the table is compatible with network message class.
--
-- @local
-- @tparam table v The table to check.
-- @bool[opt=true] checkMembers Asserts members data type.
-- @treturn bool Return true if table is compatible.
function Common.NetMessage:AssertTable ( v , checkMembers )
    assert( type( v ) == "table", "`v` expected table, got " .. type( v ) .. " instead." );
    checkMembers = (checkMembers == nil) and true or checkMembers;
    if checkMembers then
        assert( type( v.name ) == "string", "property `name` expected string, got " .. type( v.name ) .. " instead." );
        assert( type( v.syncvalue ) == "userdata", "property `syncvalue` expected userdata, got " .. type( v.syncvalue ) .. " instead." );
    end

    return true;
end


--- Constructs a network message.
--
-- **Note:** This will not register the network message.
--
-- @see Common.NetMessage:Register
-- @string name The name of this network message.
-- @treturn Common.NetMessage The new network message.
-- @usage
-- local myMsg = Common.NetMessage:Create( "MyNetMessage" );
function Common.NetMessage:Create ( name )
    -- Class properties.
    local o =
    {
        name = name
        , syncvalue = nil
    };

    setmetatable( o, baseNetMessage );

    -- Creates the communication bridge.
    local varName = string.format( "NetMsg %s" , name );
    if IsGameModule()
    then
        o.syncvalue = Game.SyncValue.Create( varName );
        o.syncvalue.value = nil;
    elseif IsUIModule()
    then
        o.syncvalue = UI.SyncValue.Create( varName );
        o.syncvalue.OnSync = function (var)
            local value = var.value;
            if type(value) ~= "string" then return; end
            local groups = string.explode( value, Common.NetMessage.GRPSEP );

            local ent = tonumber( groups[1] );
            if ent == nil then
                debug_print( "[NM]", "(OR)", "Broken ent index." );
                return;
            end

            -- Skips when this message is not for me.
            if ent ~= 0 and ent ~= UI.PlayerIndex() then
                return;
            end

            debug_print( "[NM]", "(OR)", o.name );
            local args, formats = string.explode( groups[3] , Common.NetMessage.ARGSEP ), string.explode( groups[2] , Common.NetMessage.ARGSEP );
            for i = 1, #args do
                args[i] = { value = self:ToFormatType( formats[i], args[i] ), format = formats[i] };
            end
            o:OnReceived( args );
        end
    else
        error( "No system module is loaded.", 2 )
    end

    -- Checks again.
    self:AssertTable( o );

    -- Multiple hooks.
    o = Hook:Create( table.extend( o, {} ) );

    debug_print( "[NM]", "(C)", name );
    return o;
end


--- Checks whether the network message is registered.
--
-- @string[opt] name The network message name.
-- @treturn bool Returns true if already registered.
function Common.NetMessage:IsRegistered (name)
    name = name or self.name;
    return Common.NETMSG[ string.upper(name) ] ~= nil;
end


--- Registers the network message into `Common.NETMSG` table.
--
-- The table index will be formatted as upper-case string.
--
-- @see Common.NetMessage:Create
-- @param[opt] ... Arguments will be passed to constructs a new network message.
-- @treturn Common.NetMessage This network message.
-- @usage
-- local myMsg = Common.NetMessage:Register( "MyNetMessage" );
-- print( Common.NETMSG.MYNETMESSAGE:ToString() ); -- print "MyNetMessage".
-- print( myMsg:ToString() ); -- also print "MyNetMessage".
function Common.NetMessage:Register ( ... )
    local arg = {...};
    if #arg > 0 then
        self = self:Create( ... );
    end
    assert( not self:IsRegistered() , "Duplicate netmsg name." );

    local id = string.upper(self.name);
    Common.NETMSG[ id ] = self;
    debug_print( "[NM]", "(R)", id );
    return Common.NETMSG[ id ];
end


--- Unregisters the network message from `Common.NETMSG` table.
--
-- @string[opt] name The network message name.
-- @treturn Common.NetMessage This network message.
-- @usage
-- -- you can do this,
-- myMsg:Unregister();
-- -- or you can do this,
-- Common.NetMessage:Unregister( "MyNetMessage" );
-- -- but not doing both!
function Common.NetMessage:Unregister (name)
    name = name or self.name;
    assert( self:IsRegistered(name) , "Netmsg is not registered." );

    local id = string.upper(name);
    Common.NETMSG[ id ] = nil;
    debug_print( "[NM]", "(U)", id );
    return self;
end


--- Returns the name of this network message.
--
-- @treturn string The name of this network message.
function Common.NetMessage:ToString ()
    return tostring( self.name );
end


--- Converts a value into a proper data type value.
--
-- @see Game.NetMessage:WriteAngle
-- @tparam Common.NetMessage.ARGFORMAT FormatType The format data type.
-- @tparam anything Value Any value.
-- @treturn ?anything|nil Returns a proper value. | Returns `nil` if invalid format type.
function Common.NetMessage:ToFormatType ( FormatType, Value )
    if FormatType == 'byte'
        or FormatType == 'char'
        or FormatType == 'short'
        or FormatType == 'long'
        or FormatType == 'angle'
        or FormatType == 'float'
        or FormatType == 'number'
    then
        Value = tonumber( Value );

        if FormatType == 'angle' then
            Value = Value * ( 360.0 / 256.0 );
        end

        return Value;

    elseif FormatType == 'vector' then
        return Common.Vector:FromString( tostring(Value) );

    elseif FormatType == 'string' then
        return tostring( Value );
    end

    return nil;
end


--- Metamethods
-- @section metamethod

--- Base class.
-- @tfield Common.NetMessage __index
Common.NetMessage.__index = Common.NetMessage;


--- Returns the name of this network message.
-- @tfield Common.NetMessage.ToString __tostring
Common.NetMessage.__tostring = Common.NetMessage.ToString;


--- Event Callbacks
-- @section callback

--- Called when this network message is sent to client.
--
-- **Note:** This event is called in `Game` module.
--
-- **Note:** This event is using `Hook` management.
--
-- @see Game.NetMessage:End
-- @see Common.NetMessage.ARGFORMAT
-- @tparam .netMessageArgumentProperty[] args The network message arguments.
-- @usage
-- local myMsg = Common.NetMessage:Register( "MyNetMessage" );
-- function myMsg:OnSent (args, format)
--   print (self.name .. 'is sent!' );
-- end
function Common.NetMessage:OnSent (args)
end


--- Called when this network message is received from server.
--
-- **Note:** This event is called in `UI` module.
--
-- **Note:** This event is using `Hook` management.
--
-- @see Game.NetMessage:End
-- @see Common.NetMessage.ARGFORMAT
-- @tparam .netMessageArgumentProperty[] args The network message arguments.
-- @usage
-- local myMsg = Common.NetMessage:Register( "MyNetMessage" );
-- function myMsg:OnReceived (args, format)
--   print (self.name .. 'is received!' );
-- end
function Common.NetMessage:OnReceived (args)
end

--- @section end


------------
-- Common Text Menu class.
--
-- Shared functions and properties for text menu classes.
--
-- @classmod Common.TextMenu
Common.TextMenu = {};


--- Maximum allowed menu slots to drawn.
-- @tfield[opt=10] number Common.TextMenu.maxSlot
Common.TextMenu.maxSlot = 10;


--- Valid menu slots.
--
Common.TextMenu.SLOT = {
    NUM1            =   1, -- Menu key 1.
    NUM2            =   2, -- Menu key 2.
    NUM3            =   3, -- Menu key 3.
    NUM4            =   4, -- Menu key 4.
    NUM5            =   5, -- Menu key 5.
    NUM6            =   6, -- Menu key 6.
    NUM7            =   7, -- Menu key 7.
    NUM8            =   8, -- Menu key 8.
    NUM9            =   9, -- Menu key 9.
    NUM0            =   0, -- Menu key 0.
}; Common.TextMenu.SLOT = table.readonly( Common.TextMenu.SLOT );


--- Valid menu slot bitflags.
--
Common.TextMenu.SLOTBITFLAG = {
    NUM1            =   (1<<1), -- Menu key 1.
    NUM2            =   (1<<2), -- Menu key 2.
    NUM3            =   (1<<3), -- Menu key 3.
    NUM4            =   (1<<4), -- Menu key 4.
    NUM5            =   (1<<5), -- Menu key 5.
    NUM6            =   (1<<6), -- Menu key 6.
    NUM7            =   (1<<7), -- Menu key 7.
    NUM8            =   (1<<8), -- Menu key 8.
    NUM9            =   (1<<9), -- Menu key 9.
    NUM0            =   (1<<0), -- Menu key 0.
}; Common.TextMenu.SLOTBITFLAG = table.readonly( Common.TextMenu.SLOTBITFLAG );


--- Menu status codes.
--
-- Passed on `Game.TextMenu:OnItemSelected`'s `slot` argument.
--
Common.TextMenu.STATUS = {
    NEXT            =   -1, -- Next menu page.
    BACK            =   -2, -- Previous menu page.
    EXIT            =   -3, -- Menu is closed.
    TIMEOUT         =   -4, -- Menu is closed due to display time is expired.
    REPLACED        =   -9 -- Menu is closed due to replaced with another incoming menu.
}; Common.TextMenu.STATUS = table.readonly( Common.TextMenu.STATUS );


--- Text menu key signals.
--
Common.SIGNAL.MENUKEY = {
    NUM1            =   1578534611, -- Menu key 1.
    NUM2            =   1578534612, -- Menu key 2.
    NUM3            =   1578534613, -- Menu key 3.
    NUM4            =   1578534614, -- Menu key 4.
    NUM5            =   1578534615, -- Menu key 5.
    NUM6            =   1578534616, -- Menu key 6.
    NUM7            =   1578534617, -- Menu key 7.
    NUM8            =   1578534618, -- Menu key 8.
    NUM9            =   1578534619, -- Menu key 9.
    NUM0            =   1578534610, -- Menu key 0.
    STATUS_NEXT     =   nil, -- Next page key.
    STATUS_BACK     =   nil, -- Previous page key.
    STATUS_EXIT     =   nil, -- Exit key.
    STATUS_TIMEOUT  =   nil, -- Display time is expired.
    STATUS_REPLACED =   nil, -- Replaced with another incoming menu.
};


Common.SIGNAL.MENUKEY.STATUS_NEXT       = Common.SIGNAL.MENUKEY.NUM0 + Common.TextMenu.STATUS.NEXT;
Common.SIGNAL.MENUKEY.STATUS_BACK       = Common.SIGNAL.MENUKEY.NUM0 + Common.TextMenu.STATUS.BACK;
Common.SIGNAL.MENUKEY.STATUS_EXIT       = Common.SIGNAL.MENUKEY.NUM0 + Common.TextMenu.STATUS.EXIT;
Common.SIGNAL.MENUKEY.STATUS_TIMEOUT    = Common.SIGNAL.MENUKEY.NUM0 + Common.TextMenu.STATUS.TIMEOUT;
Common.SIGNAL.MENUKEY.STATUS_REPLACED   = Common.SIGNAL.MENUKEY.NUM0 + Common.TextMenu.STATUS.REPLACED;


--- Methods
-- @section method


--- Metamethods
-- @section metamethod


--- Event Callbacks
-- @section callback

--- @section end


------------
-- Common Screen Fade class.
--
-- Shared functions and properties for screen fade classes.
--
-- @classmod Common.ScreenFade ScreenFade
Common.ScreenFade = {};

--- Screen fading flags.
--
Common.ScreenFade.FFADE = {
    IN          =   0x0000,     -- Just here so we don't pass 0 into the function.
    OUT         =   0x0001,     -- Fade out (not in).
    MODULATE    =   0x0002,     -- Modulate (don't blend).
    STAYOUT     =   0x0004,     -- ignores the duration, stays faded out until new ScreenFade message received.
    LONGFADE    =   0x0008,     -- used to indicate the fade can be longer than 16 seconds (added for czero).
}; table.readonly( Common.ScreenFade.FFADE );


--- Methods
-- @section method


--- Metamethods
-- @section metamethod


--- Event Callbacks
-- @section callback

--- @section end
--[[=========================================================
--  [COMMON] Post-loading
=========================================================--]]

-- Sets the zero vector.
Common.vecZero = table.readonly( Common.Vector:Create( 0 , 0 , 0 ) );

-- Init net messages.
Common.NetMessage:Register( "ShowMenu" );
Common.NetMessage:Register( "ScreenFade" );
Common.NetMessage:Register( "BarTime" );
Common.NetMessage:Register( "BarTime2" );

-- Init colors.
Common.COLOR = {
    YELLOWISH   = Common.Color.FromHex( 0x00FFA000 ),
    REDISH      = Common.Color.FromHex( 0x00FF1010 ),
    GREENISH    = Common.Color.FromHex( 0x0000A000 ),
};

-- Replaces global module with ours.
_G.Common = table.extend( baseCommon , Common );

-- Lock up all tables.
Hook = table.readonly( Hook );
_G.StringBuffer = table.readonly( StringBuffer );
Common.SIGNAL.MENUKEY = table.readonly( Common.SIGNAL.MENUKEY );
Common.SIGNAL = table.readonly( Common.SIGNAL );
Common.NETMSG = table.extend( table.readonly( Common.NETMSG, "Read-only built-in net msg." ), {} ); -- extended so scripters can insert new messages.
Common.COLOR = table.extend( table.readonly( Common.COLOR, "Read-only built-in color." ), {} ); -- extended so scripters can insert colors.
Common.Color = table.readonly( Common.Color );
Common.ScreenFade = table.readonly( Common.ScreenFade );
Common.TextMenu = table.readonly( Common.TextMenu );
Common.NetMessage = table.readonly( Common.NetMessage );
Common.Vector = table.readonly( Common.Vector );
Common.Vector2D = table.readonly( Common.Vector2D );
Common = table.readonly( Common );


print("[Donxon] Common is loaded.");