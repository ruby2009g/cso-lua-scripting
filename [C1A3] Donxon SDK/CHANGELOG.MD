# Changelog(s)

## 24/02/2020 (Beta)

### Globals

> * Changed `g_vecZero` -> Moved into `Common.vecZero`.
> * Changed `table.extend` -> Added `__index` support from derived tables.
> * Added `table.merge()` function.
> * Added `table.clone()` function.

### Hook

> * Changed `Hook:Create`-> Added `hookNameComparator` parameter for custom hook name bindings.

### Common

> * Changed `Common.MAX_PLAYER` -> Renamed to `Common.maxPlayer`.
> * Added `Common.vecZero` property.
> * Added `Common.emptyFunc` property.
> * Added `Common.COLOR` property.
> * Added `Common.Color` class.
> * Added `Common.ScreenFade` class.

### Common.NetMessage

> * Changed `Common.NetMessage:OnSent()` -> Removed `formats` parameter, Changed data type of `args` into `.netArgumentProperty`.
> * Changed `Common.NetMessage:OnReceived()` -> Removed `formats` parameter, Changed data type of `args` into `.netArgumentProperty`.

### Common.TextMenu

> * Changed `Common.TextMenu.MAX_SLOT` -> Renamed to `Common.TextMenu.maxSlot`.

### Game

> * Changed `Game.MONSTERTYPE` -> Extended table with all spawnable monsters.
> * Changed `Game.TextMenu` -> Added multiple-hooks registration.
> * Added `Game.GetDeltaTime()` function.
> * Added `Game.ScreenFade` class.
> * Added `Game.BarTime` class.

### Game.NetMessage

> * Changed `Game.NetMessage:Begin()` -> Added support for table options (`.netMessageBeginOption`).

### Game.TextMenu

> * Changed `Game.TextMenu.CONFIG` -> Use associative array instead of numeric.
> * Changed `Game.TextMenu.config` -> Use associative array instead of numeric.
> * Changed `Game.TextMenu:Get()` -> Added support for table options (`.textMenuConfiguration`).
> * Changed `Game.TextMenu:Set()` -> Added support for table options (`.textMenuConfiguration`).
> * Changed `Game.TextMenu:Open()` -> Added support for table options (`.textMenuOpenOption`).

### UI

> * Changed `UI.TEXT_MENU_RIGHT_ALIGN_OFFSET_X` -> Moved into `UI.TextMenu.rightAlignmentHorizontalOffset`.
> * Added `UI.GetDeltaTime()` function.
> * Added `UI.GetScreenRelativePosition()` function.
> * Added `UI.GetScreenAbsolutePosition()` function.
> * Added `UI.GetCenterPosition()` function.
> * Added `UI.Rectangle` class.
> * Added `UI.ScreenFade` class.
> * Added `UI.BarTime` class.

### UI.TextMenu

> * Changed `UI.TextMenu.SPACE_HEIGHT_AFTER_LINE` -> Renamed to `UI.TextMenu.bottomPaddingSize`.
> * Added `UI.TextMenu.rightAlignmentHorizontalOffset` property.

## 08/02/2020 (Beta)

> * Fixed dangling pointers when a hook is removed.
> * Fixed hook call-chaining bugs for not returning correct values.
> * Fixed broken `Game.Rule` methods.

## 23/01/2020 (Beta)

> * Initial public release.
