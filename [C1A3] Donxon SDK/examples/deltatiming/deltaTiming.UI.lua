txt = UI.Text.Create();
txt:Set({
        font   = medium,
        width  = 200,
        height = 50,
        r      = 255,
        g      = 255,
        b      = 255,
        a      = 255,
        x      = UI.ScreenSize().width  / 2,
        y      = UI.ScreenSize().height / 2
});
txt2 = UI.Text.Create();
txt2:Set({
        font   = medium,
        width  = 200,
        height = 50,
        r      = 255,
        g      = 255,
        b      = 255,
        a      = 255,
        x      = UI.ScreenSize().width  / 2,
        y      = UI.ScreenSize().height / 1.5
});


local MAX_VALUE = 255; -- Maximum value to reach for.
local DURATION  = 3;   -- How long until value is reached maximum vaue in second(s).


local start, current = UI.GetTime(),0;
function UI.Event:OnUpdate()
    local time  = UI.GetTime();
    local delta = UI.GetDeltaTime();


    -- Shown delta in textUI.
    txt:Set( {text = "Delta: " .. tostring( delta )} );


    -- Increments until reaching maximum value.
    if current < MAX_VALUE then
        local addValue = MAX_VALUE / DURATION;
        current = current + ( addValue * delta );
    end


    -- Reached.
    if current >= MAX_VALUE then
        -- Shown finish time, it should be same as `DURATION`.
        txt2:Set( {text = "Finish time: " .. tostring( (time - start) )} );

        -- Restart.
        start = time;
        current = 0;
    end
end

