-- Registers a 'SIMPLE:OnSent' hook.
--
function Common.NETMSG.SIMPLE:OnSent( args )
	print( "SIMPLE: OnSent" );
	for k,v in ipairs( args ) do
		print( 'Value: ', tostring(v.value) );
		print( 'Type:  ', tostring(v.format) );
		print( tostring("---------------") );
	end
end


-- Sends 'SIMPLE' net message into all players.
--
Game.NetMessage:Begin( { destination = Common.MSG.ALL, message = Common.NETMSG.SIMPLE } )
	:WriteByte( 255 )
	:WriteChar( 127 )
	:WriteShort( 32767 )
	:WriteLong( 2147483647 )
	:WriteAngle( 360 )
	:WriteCoord( 1337.321 )
	:WriteVector( Common.vecZero )
	:WriteVector( {x = 1 , y = 2 , z = 3}, true )
	:WriteString( "Hello World!" )
	:WriteEntity( Game.Player.Create(1) )
	:WriteFloat( 1234.5678 )
	:WriteNumber( 874326264239423462364234 )
:End();
