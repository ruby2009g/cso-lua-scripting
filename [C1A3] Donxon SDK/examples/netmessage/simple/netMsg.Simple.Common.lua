-- The net message will be registered as 'Common.NETMSG.<name of the net message in upper-case>'.
-- In this case, 'Common.NETMSG.SIMPLE'.
--
Common.NetMessage:Register( "SIMPLE" );
-- is same as:
-- Common.NetMessage:Create( "SIMPLE" ):Register();
