-- Registers a 'SIMPLE:OnReceived' hook.
--
function Common.NETMSG.SIMPLE:OnReceived( args )
	print( "SIMPLE: OnReceived" );
	for k,v in ipairs( args ) do
		print( 'Value: ', tostring(v.value) );
		print( 'Type:  ', tostring(v.format) );
		print( tostring("---------------") );
	end
end

