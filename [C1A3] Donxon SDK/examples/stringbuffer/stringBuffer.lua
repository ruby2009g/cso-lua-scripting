-- Creates a string buffer.
local str = StringBuffer:Create();


-- Usual string concatenations.
str = str .. "a";
str = str .. "b" .. "c";


-- Returns same value as generic string but much faster to process.
print( str:ToString() ); -- returns "abc".
