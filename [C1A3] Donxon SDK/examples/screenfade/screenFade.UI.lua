-- Draws screen fade-in to this player.
UI.ScreenFade:Draw( {
    mode = Common.ScreenFade.FFADE.IN,
    modulate = false,
    duration = 5,
    holdTime = 10,
    r = 0,
    g = 0,
    b = 0,
} );


-- Draws screen fade-out to this player.
UI.ScreenFade:Draw( {
    mode = Common.ScreenFade.FFADE.OUT,
    modulate = false,
    duration = 5,
    holdTime = 10,
    r = 0,
    g = 0,
    b = 0,
} );


-- Draws screen fade-stay to this player.
UI.ScreenFade:Draw( {
    mode = Common.ScreenFade.FFADE.STAYOUT,
    modulate = false,
    r = 0,
    g = 0,
    b = 0,
    a = 128,
} );


-- Hides the screen fade.
-- UI.ScreenFade:Hide();
