-- Normal.
local a = Common.Vector:Create( 10 , 20 , 30 );
print( a:ToString() ); -- "10 20 30"


-- Table.
a = Common.Vector:Create( {x = 10 , y = 20 , z = 30} );
print( a:ToString() ); -- "10 20 30"


-- String.
a = Common.Vector:Create( "10 20 30" );
print( a:ToString() ); -- "10 20 30"


-- operands.
a = a + {x = 30}; print( a:ToString() ); -- "40 20 30"
a = a + {y = 20}; print( a:ToString() ); -- "40 40 30"
a = a + {z = 10}; print( a:ToString() ); -- "40 40 40"
a = a - {x = 30, y = 20 , z = 10}; print( a:ToString() ); -- "10 20 30"
a = a / 2; print( a:ToString() ); -- "5 10 15"
a = a * 2; print( a:ToString() ); -- "10 20 30"


-- functions.
print( a:Length() ); -- "37.4166"
print( a:Make2D():ToString() ); -- "10 20"