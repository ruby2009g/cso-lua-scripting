-- Creates a rectangle.
foo = UI.Rectangle:Create();


-- Sets its options.
foo:Set({ 
    x = UI:GetCenterPosition().x
    , y = UI:GetCenterPosition().y
    , width = 64
    , height = 64
    , r = 0
    , g = 0
    , b = 0
    , a = 153
    , stroke = 
    {
        visible = true,
        size = 8,
        r = Common.COLOR.GREENISH.r,
        g = Common.COLOR.GREENISH.g,
        b = Common.COLOR.GREENISH.b,
        a = 255,
    }
});
-- Re-center its position.
foo:Set({ 
    x = UI:GetCenterPosition( foo:Get() ).x
    , y = UI:GetCenterPosition( foo:Get() ).y
});


-- Shows it.
foo:Show();


-- Is it visible?
print( "foo:IsVisible() => ", foo:IsVisible() );
