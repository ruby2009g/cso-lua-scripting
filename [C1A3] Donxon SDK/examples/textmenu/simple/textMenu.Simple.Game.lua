-- Creates a menu.
local customMenu = Game.TextMenu:Create( "Select a team" )
                    :AddItem( { text = "Terrorist Force" } )
                    :AddItem( { text = "Counter-Terrorist Force" } )
                    :AddItem( { text = "VIP" } )
                    :AddBlank()
                    :AddItem( { text = "Auto-select" } );


-- Sets the menu callback.
function customMenu:OnSlotSelected ( player, slot, page, item )
    print( "----------------" );
    print( "Menu #1" );
    print( "Name: ", player.name );
    print( "Slot: ", slot );
    print( "Page: ", page );
    print( "Item: ", (item and item.text or "<nothing>") );
    print( "----------------" );
end


-- Opens the menu for all players.
customMenu:Open();
