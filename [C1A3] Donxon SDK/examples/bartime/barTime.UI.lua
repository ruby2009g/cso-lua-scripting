-- Draws the progress bar to this players.
UI.BarTime:Draw( { duration = 10 } );


-- Draws the progress bar to this players with 30% already filled.
UI.BarTime:Draw( { duration = 10, startPercent = 30 } );


-- Hides the progress bars.
-- UI.BarTime:Hide();


-- Is it visible?
print( "UI.BarTime:IsVisible() => ", UI.BarTime:IsVisible() );
