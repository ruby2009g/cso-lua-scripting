-- Shows the progress bar to all players.
Game.BarTime:Show( { duration = 10 } );


-- Shows the progress bar to all players with 30% already filled.
Game.BarTime:Show( { duration = 10, startPercent = 30 } );


-- Shows the progress bar to a player with 50% already filled.
Game.BarTime:Show( { player = Game.Player.Create(1), duration = 10, startPercent = 50 } );


-- Hides all player's progress bar.
-- Game.BarTime:Hide();
