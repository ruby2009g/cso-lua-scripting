-- Score variables to be synchronized on the server
ScoreCT = UI.SyncValue.Create("ScoreCT")
ScoreTR = UI.SyncValue.Create("ScoreTR")
MaxKill = UI.SyncValue.Create("MaxKill")

-- Create scoreboard
screen = UI.ScreenSize()
center = {x = screen.width / 2, y = screen.height / 2}

scoreBG = UI.Box.Create()
scoreBG:Set({x = center.x - 135, y = 5, width = 270, height = 50, r = 10, g = 10, b = 10, a = 180})

goalBG = UI.Box.Create()
goalBG:Set({x = center.x - 35, y = 5, width = 70, height = 50, r = 0, g = 0, b = 0, a = 180})

goalLabel = UI.Text.Create()
goalLabel:Set({text='000', font='medium', align='center', x = center.x - 50, y = 17, width = 100, height = 50, r = 255, g = 255, b = 255})

ctLabel = UI.Text.Create()
ctLabel:Set({text='000', font='large', align='left', x = center.x - 127, y = 18, width = 100, height = 50, r = 100, g = 100, b = 255})

trLabel = UI.Text.Create()
trLabel:Set({text='000', font='large', align='right', x = center.x + 26, y = 18, width = 100, height = 50, r = 255, g = 80, b = 80})

-- Scoreboard updates whenever variable is synchronized
function ScoreCT:OnSync()
    local str = string.format("%03d", self.value)
    ctLabel:Set({text = str})
end

function ScoreTR:OnSync()
    local str = string.format("%03d", self.value)
    trLabel:Set({text = str})
end

function MaxKill:OnSync()
    local str = string.format("%03d", self.value)
    goalLabel:Set({text = str})
end