local TDM = Game.Rule
TDM.name = "Team Deathmatch"
TDM.desc = "Timed Modes created by script"

-- Unable to destroy map
TDM.breakable = false

-- Goal Score
local MaxKill = Game.SyncValue.Create("MaxKill")
MaxKill.value = 100

-- Team score
local Score = {}
Score[Game.TEAM.CT] = Game.SyncValue.Create("ScoreCT")
Score[Game.TEAM.CT].value = 0
Score[Game.TEAM.TR] = Game.SyncValue.Create("ScoreTR")
Score[Game.TEAM.TR].value = 0

-- Starting Max Health
function TDM:OnPlayerConnect(player)
    player.maxhealth = 100
end

function TDM:OnPlayerSpawn(player)
    player:ShowBuymenu()
end

function TDM:OnPlayerKilled(victim, killer)
    -- Suicide
    if killer == nil then
        return
    end

    -- 1 point to the killing player team
    local killer_team = killer.team
    local point = Score[killer_team]
    point.value = point.value + 1

    -- Decreases maximum health by 10 each time you kill!
    killer.maxhealth = killer.maxhealth - 10

    -- Increases maximum health by 10 each time you die!
    victim.maxhealth = victim.maxhealth + 10

    -- Win if you overcome the goal!
    if (point.value >= MaxKill.value) then
        self:Win(killer_team)
    end
end