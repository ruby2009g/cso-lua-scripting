--[[

    Speed Record plugin.
    Version 01/03/2019.

    Description:
    A Lua addon for Speedometer plugin.
    The purpose is to tracking player's best speed records and also the best of all players speed record.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Remove unecessary table(s).
SpeedoMeter.Game = nil;


-- Local variables.
local module = SpeedoMeter.UI;
local txt = SpeedoMeter.UI.Text;
local debug = SpeedoMeter.Debug;
local syncvalue = SpeedoMeter.SyncValue;
local playerBestSpeed = -1;
local playerBestOfAllSpeed = -1;


-- SyncValue init(s).
syncvalue.PlayerSpeedBestAll.Instance = UI.SyncValue.Create( syncvalue.PlayerSpeedBestAll.NAME );


-- UI element init(s).
local txtElement    = txt.PlayerSpeedBest;
local txtConfig     = txtElement.Config;
txtConfig.format    = txtConfig.text;
txtConfig.x         = 32;
txtConfig.y         = 0;
txtElement.Instance = UI.Text.Create();
txtElement.Instance:Set( txtConfig );
txtElement.Instance:Hide();

txtElement          = txt.PlayerSpeedBestAll;
txtConfig           = txtElement.Config;
txtConfig.format    = txtConfig.text;
txtConfig.x         = txt.PlayerSpeedBest.Config.x;
txtConfig.y         = txt.PlayerSpeedBest.Config.y + (txt.PlayerSpeedBest.Config.height/2);
txtElement.Instance = UI.Text.Create();
txtElement.Instance:Set( txtConfig );
txtElement.Instance:Hide();


-- Returns the player's best speed record.
function module:GetBestSpeed()
    return playerBestSpeed;
end


-- Returns the best of all players speed record.
function module:GetBestOfAllSpeed()
    return playerBestOfAllSpeed;
end


--[[ from this link: http://lua-users.org/wiki/SplitJoin ]]
-- Explodes a string into a table.
function module:explode( source , delimiter )
    local t, ll
    t={}
    ll=0
    if(#source == 1) then
        return {source}
    end
    while true do
        l = string.find(source, delimiter, ll, true) -- find the next d in the string
        if l ~= nil then -- if "not not" found then..
            table.insert(t, string.sub(source,ll,l-1)) -- Save it in our array.
            ll = l + 1 -- save just after where we found it for searching next time.
        else
            table.insert(t, string.sub(source,ll)) -- Save what's left in our array.
            break -- Break at end, as it should be, according to the lua manual.
        end
    end
    return t
end


-- PlayerSpeedBestAll OnSync hook.
function syncvalue.PlayerSpeedBestAll:OnSync()
    local txtValue = "(none).";
    
    if syncvalue.PlayerSpeedBestAll.Instance ~= nil
    then
        txtValue = syncvalue.PlayerSpeedBestAll.Instance.value;
        playerBestOfAllSpeed = tonumber( module:explode( txtValue , "\n" )[2] );
    else
        playerBestOfAllSpeed = -1;
    end

    local txtElement    = txt.PlayerSpeedBestAll;
    local txtConfig     = txtElement.Config;
    local txtInstance   = txtElement.Instance;
    txtInstance:Set( { text = string.format( txtConfig.format , txtValue ) } );
    txtInstance:Show();
end


-- OnUpdate hook.
function module:OnUpdate()
    local value = -1;

    ---[[
    if module:GetSpeed() ~= nil
    then
        value = module:GetSpeed();
    end
    --]]

    if value > playerBestSpeed
    then
        playerBestSpeed = value;
    end

    local txtElement    = txt.PlayerSpeedBest;
    local txtConfig     = txtElement.Config;
    local txtInstance   = txtElement.Instance;
    txtInstance:Set( { text = string.format( txtConfig.format , playerBestSpeed ) } );
    txtInstance:Show();
end


-- Default hook executions.
function syncvalue.PlayerSpeedBestAll.Instance:OnSync()
    syncvalue.PlayerSpeedBestAll:OnSync();
end
function UI.Event:OnUpdate( time )
    module:OnUpdate();
end


print( "UI.SpeedRecord.Main is loaded!" );

