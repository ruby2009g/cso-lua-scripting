--[[

    Speedometer plugin.
    Version 01/03/2019.

    Description:
    A Lua plugin for displaying player's movement speed.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Remove unecessary table(s).
SpeedoMeter.UI = nil;


-- Local variables.
local debug = SpeedoMeter.Debug;
local module = SpeedoMeter.Game;
local modulePlayer = SpeedoMeter.Game.Player;
local syncvalue = SpeedoMeter.SyncValue;


-- Creates all speed sync variables.
for i = 1, 32
do
    local varName = string.format( syncvalue.PlayerSpeed.NAME , i );

    syncvalue.PlayerSpeed.Instances[i] = Game.SyncValue.Create( varName );

    if syncvalue.PlayerSpeed.Instances[i] ~= nil
    then
        debug:print( string.format("[Game] %s", varName) );
    end
end


-- Returns the player's speed.
function modulePlayer:GetSpeed( index )
    local var = syncvalue.PlayerSpeed.Instances[ index ];
    return var ~= nil and (var.value ~= nil and var.value or -1) or -1;
end


-- Gets a player object by player's index.
function modulePlayer:GetByIndex( index )
    player = nil;
    player = Game.Player.Create( index );
    return player;
end
    

-- Checks whether the player is alive.
function modulePlayer:IsAlive( player )
    return player ~= nil and player.health > 0;
end


--[[ from this link: https://github.com/ValveSoftware/halflife/blob/master/dlls/vector.h#L81 ]]
-- Gets the length of a Vector table.
function module:VectorLength( table , is3D )
    local x,y,z = table.x , table.y , ( is3D and table.z or 0 );
    return math.sqrt( x*x + y*y + z*z );
end


-- OnUpdate hook.
function module:OnUpdate( time )
    -- Updates player speed value.
    for k,v in ipairs( syncvalue.PlayerSpeed.Instances )
    do
        local player = modulePlayer:GetByIndex( k );
        if modulePlayer:IsAlive( player )
        then
            v.value = self:VectorLength( player.velocity , self.IS_3D_MOVEMENT );
        end
    end
end


-- Default hook executions.
function Game.Rule:OnUpdate( time )
    module:OnUpdate( time );
end


print( "Game.Speedometer.Main is loaded!" );

