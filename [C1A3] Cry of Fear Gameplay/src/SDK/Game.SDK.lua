-- Map-exclusive game script file.

-- Boolean variables to triggers special COF trigger name.
local isEndIntroAllowed = false;
local isEndConclusionAllowed = false;
local isEndCreditAllowed = false;


-- COF hookable events.
function COF.Game.Event:OnPrologue( player , onoff )
    if not onoff and isEndIntroAllowed
    then
        isEndIntroAllowed = false;
        Game.SetTrigger( "EndIntro" , true );
    end
end
function COF.Game.Event:OnEpilogue( player , onoff )
    if not onoff and isEndConclusionAllowed
    then
        isEndConclusionAllowed = false;
        Game.SetTrigger( "EndConclusion" , true );
    end
end
function COF.Game.Event:OnCredit( player , onoff )
    if not onoff and isEndCreditAllowed
    then
        isEndCreditAllowed = false;
        Game.SetTrigger( "EndCredit" , true );
    end
end


-- Intro scene.
function Intro_Start( onoff )
    if not onoff then return; end

    COF.Game:SetCutscene( true );
    COF.Game:DrawScreenFade( COF.Enum.FADEMODE.SET , nil , 255 , 255 , 255 , 255 );
end
function Intro_001( onoff )
    if not onoff then return; end

    COF.Game:DrawScreenFade( COF.Enum.FADEMODE.IN , 1 );
end
function Intro_002( onoff )
    if not onoff then return; end

    COF.Game:ShowPrologue( true );
end
function Intro_003( onoff )
    if not onoff then return; end

    COF.Game:ShowPrologue( false );
end
function Intro_004( onoff )
    if not onoff then return; end

    isEndIntroAllowed = true;
end
function Intro_End( onoff )
    if not onoff then return; end

    COF.Game:DrawScreenFade( COF.Enum.FADEMODE.OUT );
    COF.Game:SetCutscene( false );
end
function ShowChapterTitle( onoff )
    if not onoff then return; end

    COF.Game:SetChapter( 1 , "Lost in a SDK" );
    COF.Game:SetObjective( "Reach to the surface." );
end


-- Ending scene.
function FadeToWhite( onoff )
    if not onoff then return; end

    COF.Game:DrawScreenFade( COF.Enum.FADEMODE.IN , 2.5 , 255 , 255 , 255 , 255 );
end
function Conclusion_Start( onoff )
    if not onoff then return; end

    COF.Game:SetCutscene( true );
    COF.Game:DrawScreenFade( COF.Enum.FADEMODE.IN );
end
function Conclusion_001( onoff )
    if not onoff then return; end

    isEndConclusionAllowed = true;
    COF.Game:ShowEpilogue([[
#Conclusion

"Hello! This is AnggaraNothing speaking, I'd 

like to make sure that I'm NOT affiliated with, 

funded, or in any way associated with the organization 

known as Team Psykskallar and also "Cry of Fear" title and 

all assets used in-game are properties of Team Psykskallar 

and the credited content producers.

Besides all of that, Lua scripts are fully made from scratch 

by myself. This SDK map concepts are taken from original

Cry of Fear SDK sample map. You are freely to use these scripts

and this SDK map but please do not remove the copyright notices

on all of my scripts.



I'd like to thanks to Team Psykskallar for released such

a great game and deserved to awarded with Game of the Year award!

I'd like to recommends you to play their game on Steam,

it's free to play and worths your time to play it.



If you need more help about Lua, you can contact me

at CSNZ, Youtube channel or CSO Wikia discord server.

You can get the latest script version on my gitlab repo,

and for unofficial Lua API docs, you can access my gitlab wiki

at this link, bit.ly/ANCSOLua



Thank you, and happy modding!"]]);
end
function Conclusion_End( onoff )
    if not onoff then return; end

    COF.Game:ShowEpilogue( '' );
end


-- Credits scene.
function Credit_Begin( onoff )
    if not onoff then return; end

    COF.Game:SetCutscene( true );
    COF.Game:DrawScreenFade( COF.Enum.FADEMODE.SET );

    isEndCreditAllowed = true;
    COF.Game:ShowCredit( true );
end


-- "Tape Recorder" hint.
function SeeTapeRecorder( onoff )
    if not onoff then return; end

    COF.Game:EnqueueHint( "Use the Tape Recorder to set a new spawn point." );
end


-- "New spawnpoint" subtitle.
function NewSpawnpoint( onoff )
    if not onoff then return; end

    COF.Game:SetSubtitle( "GAME_SAVED" );
end


-- "Door unlocked" subtitle.
function DoorUnlocked( onoff )
    if not onoff then return; end

    COF.Game:SetSubtitle( "You unlocked the lock." );
end


-- "Door locked #1" subtitle.
function DoorLocked001( onoff )
    if not onoff then return; end

    COF.Game:SetSubtitle( "I can't open it. Strange, there's no lock..." );
end


-- "Door locked #2" subtitle.
function DoorLocked002( onoff )
    if not onoff then return; end

    COF.Game:SetSubtitle( "The door won't budge." );
end

