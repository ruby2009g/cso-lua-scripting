--[[

    Cry of Fear Gameplay.
    Version 04/20/2019.

    Description:
    Cry of Fear gameplay system for CSO Studio.


        MIT License

        Copyright (c) 2019 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Debug mode.
COF.Debug.ENABLED = true;


-- Game settings.
if Game ~= nil then

-- Enable/disable ground velocity changes when on cutscene.
COF.Game.CUTSCENE_FREEZE_MOVEMENT_ENABLED = false;


-- Enable/disable sprint.
COF.Game.SPRINT_ENABLED = true;


-- Sprint movement speed.
COF.Game.SPRINT_SPEED = 280;


-- Enable/disable dodge.
COF.Game.DODGE_ENABLED = true;


-- Dodge movement speed.
COF.Game.DODGE_SPEED = 500;


-- Enable/disable double-tap dodging.
COF.Game.DODGE_TAP_ENABLED = true;


-- Double-tap dodging, time between taps in milisecond(s).
COF.Game.DODGE_TAP_INTERVAL_MIN = COF.FASTEST_RECEIVED_DOUBLETAP_INPUT_TIME;
COF.Game.DODGE_TAP_INTERVAL_MAX = 110;


-- Dodge cooldown in second(s).
COF.Game.DODGE_COOLDOWN = 1;


-- Sprint stamina cost for each 0.1 second.
COF.Game.STAMINA_SPRINT_COST = 1.67;


-- Stamina cost for each dodge.
COF.Game.STAMINA_DODGE_COST = 12.5;


-- Stamina cost for each jump.
COF.Game.STAMINA_JUMP_COST = 10;


-- Stamina recovery cooldown time if depleted in second(s).
COF.Game.STAMINA_RECOVER_DEPLETED_COOLDOWN = 5;


-- Stamina recovery rates while on standing for each 0.1 second.
COF.Game.STAMINA_RECOVER_STANDING = 0.4;


-- Disabled because we can't detect player ducking yet. --Anggara_nothing
-- Stamina recovery rates while on crouch for each 0.1 second.
--COF.Game.STAMINA_RECOVER_CROUCH = 0.83;


-- New received phone message trigger name.
-- Usually used for triggering new unread sms sound fx.
COF.Game.PHONE_MESSAGE_NEW_TRIGGER_NAME = "NewSMS";
end


-- UI settings.
if UI ~= nil then
local size = UI.ScreenSize();
local center = { x = size.width/2 , y = size.height/2 };

-- Screen fade settings.
COF.UI.Box.ScreenFade.Config =
{
    DEFAULT =
    {
        fademode    = COF.Enum.FADEMODE.IN
        , faderate  = 10
        , r         = 20
        , g         = 20
        , b         = 20
        , a         = 255
    }

    , width  = size.width  + 40
    , height = size.height + 40
    , x = -20
    , y = -20
};


-- Chapter title settings.
COF.UI.Text.Chapter.Config =
{
    holdtime = 5
    , faderate    = 10
    , font      = 'medium'
    , align     = 'center'
    , width  = size.width * 0.75
    , height = COF.TEXT_MEDIUM_HEIGHT
    , r = 255
    , g = 255
    , b = 255
    , x = center.x - (size.width * 0.75 * 0.5)
    , y = center.y - (COF.TEXT_MEDIUM_HEIGHT * 0.5)
    , format = "Chapter %s : %s"
};
COF.UI.Box.Chapter.Config =
{
    faderate      = 10
    , slideuprate = 20
    , height = 4
    , r = 255
    , g = 255
    , b = 255
    , x = center.x - (COF.UI.Text.Chapter.Config.width * 0.5)
    , y = center.y + COF.UI.Text.Chapter.Config.height + 4
};


-- HUD Health bar settings.
COF.UI.Text.Icon.Health.Config =
{
    font        = 'medium'
    , align     = 'left'
    , text      = '+'
    , width  = COF.TEXT_MEDIUM_WIDTH + 5
    , height = COF.TEXT_MEDIUM_HEIGHT + 5
    , r = 139
    , g = 0
    , b = 0
    , a = 255
    , x = COF.HUD_PADDING - 3
    , y = size.height - COF.HUD_PADDING - COF.HUD_ICON_CROSS_HEIGHT + 5
};
COF.UI.Box.Health.Border.Config =
{
    padding = 4
    , width  = COF.HUD_ICON_NUMERIC_WIDTH * 9
    , height = COF.HUD_ICON_NUMERIC_HEIGHT + 3
    , r = 255
    , g = 255
    , b = 255
    , a = COF.UI.Text.Icon.Health.Config.a
    , x = COF.HUD_PADDING + COF.HUD_ICON_CROSS_WIDTH - 3
    , y = size.height - COF.HUD_PADDING - COF.HUD_ICON_CROSS_HEIGHT
};
COF.UI.Box.Health.Background.Config =
{
    width  = COF.UI.Box.Health.Border.Config.width - COF.UI.Box.Health.Border.Config.padding * 2
    , height = COF.UI.Box.Health.Border.Config.height - COF.UI.Box.Health.Border.Config.padding * 2
    , r = 0
    , g = 0
    , b = 0
    , a = COF.UI.Text.Icon.Health.Config.a
    , x = COF.UI.Box.Health.Border.Config.x + COF.UI.Box.Health.Border.Config.padding
    , y = COF.UI.Box.Health.Border.Config.y + COF.UI.Box.Health.Border.Config.padding
};
COF.UI.Box.Health.Current.Config =
{
    slideuprate = 15
    , height = COF.UI.Box.Health.Background.Config.height
    , r = 139
    , g = 0
    , b = 0
    , a = COF.UI.Text.Icon.Health.Config.a
    , x = COF.UI.Box.Health.Background.Config.x
    , y = COF.UI.Box.Health.Background.Config.y
};
COF.UI.Box.Health.Limit.Config =
{
    height = COF.UI.Box.Health.Background.Config.height
    , r = 128
    , g = 128
    , b = 128
    , a = COF.UI.Text.Icon.Health.Config.a
    , x = COF.UI.Box.Health.Background.Config.x + COF.UI.Box.Health.Background.Config.width
    , y = COF.UI.Box.Health.Background.Config.y
};


-- HUD Stamina bar settings.
COF.UI.Text.Icon.Stamina.Config =
{
    font        = 'medium'
    , align     = 'left'
    , text      = '~'
    , width  = COF.TEXT_MEDIUM_WIDTH + 10
    , height = COF.TEXT_MEDIUM_HEIGHT + 5
    , r = 0
    , g = 0
    , b = 139
    , a = 255
    , x = COF.HUD_PADDING + COF.HUD_ICON_CROSS_WIDTH + COF.HUD_ICON_NUMERIC_WIDTH * 7 + (size.width/COF.HUD_PADDING) - 8
    , y = size.height - COF.HUD_PADDING - COF.HUD_ICON_CROSS_HEIGHT + 5
};
COF.UI.Box.Stamina.Border.Config =
{
    padding = 4
    , width  = COF.HUD_ICON_NUMERIC_WIDTH * 9
    , height = COF.HUD_ICON_NUMERIC_HEIGHT + 3
    , r = 255
    , g = 255
    , b = 255
    , a = COF.UI.Text.Icon.Stamina.Config.a
    , x = COF.HUD_PADDING + COF.HUD_ICON_CROSS_WIDTH + COF.HUD_ICON_NUMERIC_WIDTH * 7 + (size.width/COF.HUD_PADDING) + COF.HUD_ICON_SUIT_WIDTH - 3
    , y = size.height - COF.HUD_PADDING - COF.HUD_ICON_CROSS_HEIGHT
};
COF.UI.Box.Stamina.Background.Config =
{
    width  = COF.UI.Box.Stamina.Border.Config.width - COF.UI.Box.Stamina.Border.Config.padding * 2
    , height = COF.UI.Box.Stamina.Border.Config.height - COF.UI.Box.Stamina.Border.Config.padding * 2
    , r = 0
    , g = 0
    , b = 0
    , a = COF.UI.Text.Icon.Stamina.Config.a
    , x = COF.UI.Box.Stamina.Border.Config.x + COF.UI.Box.Stamina.Border.Config.padding
    , y = COF.UI.Box.Stamina.Border.Config.y + COF.UI.Box.Stamina.Border.Config.padding
};
COF.UI.Box.Stamina.Current.Config =
{
    slideuprate = 15
    , fadeoutrate = 15
    , height = COF.UI.Box.Stamina.Background.Config.height
    , r = 0
    , g = 0
    , b = 139
    , a = COF.UI.Text.Icon.Stamina.Config.a
    , x = COF.UI.Box.Stamina.Background.Config.x
    , y = COF.UI.Box.Stamina.Background.Config.y
};


-- Phone screen settings.
COF.UI.Box.Phone.Screen.Config =
{
    padding  = 4
    , width  = 200
    , height = 220
    , r = 148
    , g = 156
    , b = 172
    , a = 255
};


-- Phone border settings.
COF.UI.Box.Phone.Border.Config =
{
    thickness = 3
    , r = 0
    , g = 0
    , b = 0
    , a = 255
};


-- Phone screen overlay settings.
COF.UI.Box.Phone.Overlay.Config = 
{
    faderate = 5
    , r = 0
    , g = 0
    , b = 0
};


-- Cutscene box settings.
COF.UI.Box.Cutscene.Config = 
{
    sliderate   = 2
    , width     = size.width
    , height    = COF.HUD_PADDING + COF.HUD_ICON_CROSS_HEIGHT
    , r         = 0
    , g         = 0
    , b         = 0
    , a         = 255
};


-- Phone tooltip text settings.
COF.UI.Text.Tooltip.Phone.Config =
{
    font        = 'small'
    , align     = 'left'
    , width     = COF.TEXT_SMALL_WIDTH * 24
    , height    = COF.TEXT_SMALL_HEIGHT * 3
    , r         = 255
    , g         = 255
    , b         = 255

    , Default =
    {
        text      = 'Press T key to\nopen/close phone screen.'
    }
    , New =
    {
        text      = 'New message is received.\nPress T key to\nopen/close phone screen.'
    }
    , NotAvailable =
    {
        text      = 'Phone battery is empty.'
    }
};


-- Enable/disable phone new SMS alert.
COF.UI.Text.Phone.NewMessage.ENABLED = true;


-- Phone new SMS alert text settings.
COF.UI.Text.Phone.NewMessage.Config =
{
    font        = 'medium'
    , x         = center.x - (COF.TEXT_MEDIUM_WIDTH * 32/2)
    , y         = size.height * 0.65
    , align     = 'center'
    , width     = COF.TEXT_MEDIUM_WIDTH * 32
    , height    = COF.TEXT_MEDIUM_HEIGHT * 2
    , r         = 0
    , g         = 200
    , b         = 0
    , holdtime  = 4.5
    , text      = '      New message is received\nPress T key to open phone screen'
};


-- Phone message subject text settings.
COF.UI.Text.Phone.Subject.Config =
{
    font        = 'small'
    , align     = 'center'
    , r         = 0
    , g         = 0
    , b         = 0
};


-- Phone message text settings.
COF.UI.Text.Phone.Message.Config =
{
    font        = 'small'
    , align     = 'left'
    , r         = 0
    , g         = 0
    , b         = 0
};


-- Phone sub-menu options text settings.
COF.UI.Text.Phone.Options.Config =
{
    font        = 'small'
    , align     = 'left'
    , width     = COF.TEXT_SMALL_WIDTH * 7
    , r         = 0
    , g         = 0
    , b         = 0
    , text      = 'Options'
};


-- Phone sub-menu back text settings.
COF.UI.Text.Phone.Back.Config =
{
    font        = 'small'
    , align     = 'left'
    , width     = COF.TEXT_SMALL_WIDTH * 4
    , r         = 0
    , g         = 0
    , b         = 0
    , text      = 'Back'
};


-- Subtitle text settings.
COF.UI.Text.Subtitle.Config = 
{
    faderate    = 20
    , font      = 'small'
    , align     = 'center'
    , height    = COF.TEXT_SMALL_HEIGHT
    , r         = 255
    , g         = 255
    , b         = 255
};


-- Hint settings.
COF.UI.Box.Hint.Config =
{
    padding = 4
    , holdtime = 5
    , sliderate = 5
    , width  = 800
    , height = 40
    , r = 50
    , g = 44
    , b = 44
    , a = 255
};
COF.UI.Text.Hint.Config =
{
    font     = 'small'
    , align  = 'center'
    , width  = COF.UI.Box.Hint.Config.width  - COF.UI.Box.Hint.Config.padding * 2
    , height = COF.UI.Box.Hint.Config.height - COF.UI.Box.Hint.Config.padding * 2
    , r = 255
    , g = 255
    , b = 255
    , a = 255
    , text = "Hint: MODDING claims 1000 lives every year."
};


-- Objective tooltip text settings.
COF.UI.Text.Tooltip.Objective.Config =
{
    font        = 'small'
    , align     = 'left'
    , width     = COF.TEXT_SMALL_WIDTH * 24
    , height    = COF.TEXT_SMALL_HEIGHT * 3
    , r         = 255
    , g         = 255
    , b         = 255
    , text      = 'Press B key to\nview current objective.'
};


-- Prologue text settings.
COF.UI.Text.Prologue.Config =
{
    faderate  = 10
    , font      = 'medium'
    , align     = 'center'
    , height    = COF.TEXT_MEDIUM_HEIGHT + 4
    , r = 255
    , g = 255
    , b = 255
};


-- Epilogue text settings.
COF.UI.Text.Epilogue.Config =
{
    scrolluprate    = 1
    , faderate      = 10
    , yTop          = COF.HUD_PADDING + COF.HUD_ICON_CROSS_HEIGHT + COF.HUD_PADDING + COF.HUD_PADDING
    , yBottom       = size.height - COF.HUD_PADDING - COF.HUD_ICON_CROSS_HEIGHT - COF.TEXT_MEDIUM_HEIGHT
    , font          = 'small'
    , height        = COF.TEXT_SMALL_HEIGHT
    , fontHeader    = 'medium'
    , heightHeader  = COF.TEXT_MEDIUM_HEIGHT
    , align         = 'center'
    , r = 255
    , g = 255
    , b = 255
    , a = 255
};


-- Credits text settings.
COF.UI.Text.Credit.Config =
{
    scrolldownrate    = 1
    , faderate      = 10
    , yTop          = COF.HUD_PADDING + COF.HUD_ICON_CROSS_HEIGHT + COF.TEXT_MEDIUM_HEIGHT
    , yBottom       = size.height - COF.HUD_PADDING - COF.HUD_ICON_CROSS_HEIGHT - COF.TEXT_MEDIUM_HEIGHT
    , font          = 'small'
    , height        = COF.TEXT_SMALL_HEIGHT
    , fontHeader    = 'medium'
    , heightHeader  = COF.TEXT_MEDIUM_HEIGHT
    , align         = 'center'
    , r = 255
    , g = 255
    , b = 255
    , a = 255
};
end


-- Phone message list.
-- Message with ID, subject, and text.
COF.PhoneMsg =
{
    DEFAULT =
    {
        EnableTextWrap = true
        , Subject = ""
        , Message = ""
    }

    , TEST001 =
    {
        EnableTextWrap = true
        , Subject = "AnggaraNothing"
        , Message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    }

    , TEST002 =
    {
        EnableTextWrap = true
        , Subject = "AnggaraNothing"
        , Message = "Loremipsumdolorsitamet,consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    }

    , EASTEREGG =
    {
        EnableTextWrap = true
        , Subject = "AnggaraNothing"
        , Message = "Help me! Dean locked me down in his basement and forced me to work on his project!"
    }

    , Mom001 =
    {
        EnableTextWrap = false
        , Subject = "Mom"
        , Message = "Where are you?\nCome home as soon\nas possible, it's\ngetting dark."
    }

    , Mom002 =
    {
        EnableTextWrap = false
        , Subject = "Mom"
        , Message = "Are you okay?\nYou better come\nhome now, I'm\ngetting worried."
    }

    , IntoDarkness =
    {
        EnableTextWrap = false
        , Subject = "+46032738"
        , Message = "Look to the\nright."
    }

    , Apartment001 =
    {
        EnableTextWrap = false
        , Subject = "+46098372"
        , Message = "Help me!! Please\nhelp!! They are\ncoming! I'm\ninside the\napartments,\nhurry!!"
    }

    , Apartment002 =
    {
        EnableTextWrap = false
        , Subject = "+46098372"
        , Message = "Fourth floor!!"
    }

    , Apartment003 =
    {
        EnableTextWrap = false
        , Subject = "+46098372"
        , Message = "In here."
    }

    , Apartment004 =
    {
        EnableTextWrap = false
        , Subject = "+46370282"
        , Message = "Take the stairs,\nup, up, up. I've\ngot something to\nshow you"
    }

    , LowBattery =
    {
        EnableTextWrap = false
        , Subject = "Warning"
        , Message = "Low Battery"
    }
};


-- Hint text list.
-- Hint with ID and a list of line text.
COF.Hint =
{
    suicide = 
    {
        Lines = 
        {
            "Tap (LEFT MOUSE) to resist the suicidal influence!"
        }
    }

    , sprint = 
    {
        Lines = 
        {
            "Hold (SHIFT + W) to sprint. The blue bar on the HUD is your stamina bar."
            , "Sprinting will deplete your stamina bar quickly, so use it sparingly."
            --, "Your stamina will slowly recover while not sprinting or jumping. Crouching recovers faster."
            , "Your stamina will slowly recover while not sprinting or jumping."
        }
    }

    , nightvision = 
    {
        Lines = 
        {
            "Press (N) to turn nightvision on/off."
            , "Nightvision does not require batteries."
        }
    }

    , inventory = 
    {
        Lines = 
        {  
            "Press (I) to access your inventory."
            -- , "From here you can use, drop, equip or combine items."
            , "From here you can use, drop items."
            -- , "Some items can be combined with weapons and vice versa. Mouse-over slots for hints."
        }
    }

    , quickslots = 
    {
        Lines = 
        {
            "You can put up to 3 items in your quick slots."
            , "If you press * you will equip the weapon in your first quick slot."
            , "This is faster than equipping through the inventory and can save your life."
        }
    }

    , melee = 
    {
        Lines = 
        {
            "Press (RIGHT MOUSE) to perform a melee attack with your weapon."
            , "This will cause a small amount of damage to monsters in your immediate vicinity."
            , "Use it to push attack enemies when surrounded, or out of ammo."
        }
    }

    , ladders = 
    {
        Lines = 
        {
            "Press (E) to climb ladders. Once on the ladder, use directional keys to climb/descend."
        }
    }

    , dodge = 
    {
        Lines = 
        {
            "Double tap directional keys or hold (SHIFT) to dodge in a specific direction."
            , "This is extremely useful for avoiding attacks and can save your life."
            , "Dodging requires stamina, so use it wisely."
        }
    }

    , mobilelight = 
    {
        Lines = 
        {
            "Your mobile phone can be used as a source of light for navigating dark areas."
            --, "Press (F) to activate your light. The mobile light does not require batteries."
            , "Press (F) to activate your light. The mobile light does require batteries."
            --, "If you holster your mobile with the light on, a dim light will still shine through your bag."
        }
    }

    , useitems = 
    {
        Lines = 
        {
            "To use a key item, open your inventory with (I)."
            -- , "From here, click the use button and then click the key item."
            , "From here, click the key item and then click the use button."
            -- , "Remember to check the key item for clues if you're unsure where to use it."
        }
    }

    , usemobilephone = 
    {
        Lines = 
        {
            "To call a number with your mobile, press *."
            , "From here, you can click the numbers or type them with a keyboard."
            , "When you've entered the number, press the send button. Some numbers will trigger events."
        }
    }

    , save = 
    {
        Lines = 
        {
            "You can save your game at any location with a tape recorder."
            , "You are allowed to save up to 5 individual saves for multiple playthroughs."
            , "Saving requires a tape in Nightmare mode. The tape can only be used 5 times."
        }
    }

    , ironsights = 
    {
        Lines = 
        {
            "Press * to toggle ironsights."
            , "Iron sights allow you to aim precisely at the expense of limited movement."
        }
    }

    , weaponfunctionglock = 
    {
        Lines = 
        {
            "Press * to toggle taclight on/off."
            , "The taclight does not require any batteries."
        }
    }

    , weaponfunctionm16 = 
    {
        Lines = 
        {
            "Press * to toggle between single shot and burst fire."
            , "Burst fire will fire 3 successive rounds at an extremely fast rate."
            , "Burst fire comes at a price of higher difficulty in controlling the weapon."
        }
    }
        
    , crouch = 
    {
        Lines = 
        {
            "Hold (CTRL) to crouch. When crouched, you can navigate into tight areas."
            --, "Crouching also helps recover your stamina faster, but you cannot move as fast."
        }
    }

    , objectives = 
    {
        Lines = 
        {
            "Press (B) to view current objective."
        }
    }

    -- Post Steam release stuff.
    , weaponfunctionsniperbreathe = 
    {
        Lines = 
        {
            "Press (SHIFT) to hold your breath and steady your aim."
            , "This requires stamina."
        }
    }
};


-- Subtitle text list.
-- Subtitle with ID, text, duration, and color.
COF.Subtitle =
{
    DEFAULT =
    {
        text        = ""
        , holdtime  = 5
        , r         = 255
        , g         = 255
        , b         = 255
    }
};


print( "COF.Configuration is loaded!" );

