--[[

    Team Swapping library.
    Version 28/12/2018.

    Description:
    A Lua library to swaps the CT and TR on spawn at predefined position(s).


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


local plrSpawnPos = {};
local isTerrorist = false;
TeamSwap = {};

-- Insert the swap origin.
function TeamSwap:InsertPosition( position )
    table.insert( plrSpawnPos , position );
    print( string.format("[Game] Swap(%s; %s; %s)." , tostring(position.x), tostring(position.y), tostring(position.z)) );
end


-- Get the swap origin.
function TeamSwap:GetPosition( index )
    return plrSpawnPos[index];
end
    

-- Check whether swap origin list is empty.
function TeamSwap:IsEmpty()
    return #plrSpawnPos <= 0;
end


-- Set the next team to swap.
function TeamSwap:SetNextTeam( team )
    isTerrorist = ( team == Game.TEAM.TR );
end


-- Get the next team to swap.
function TeamSwap:GetNextTeam()
    return isTerrorist and Game.TEAM.TR or Game.TEAM.CT;
end


-- Swap the team immediately.
function TeamSwap:DoSwap()
    if self:IsEmpty()
    then
        print( "[Game] Swap position is empty." );
        return;
    end

    for i = 1, 32
    do
        local player = Game.Player.Create( i );
        if player ~= nil
        then
            if player.team == self:GetNextTeam()
            then
                local index = math.random(1 , #plrSpawnPos);
                player.position = self:GetPosition( index );
                print( string.format("[Game] SwapPlr(%s,%i)." , player.name , index) );
            end
        end
    end

    -- Switch to the next team.
    isTerrorist = not isTerrorist;
end


print( "Game.TeamSwap.Main is loaded!" );

