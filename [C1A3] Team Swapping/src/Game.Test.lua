--[[

    Team Swapping library.
    Version 28/12/2018.

    Description:
    A demonstration of Team Swapping library.


        MIT License

        Copyright (c) 2018 Anggara Yama Putra

        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:

        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.

]]


-- Firstly, insert the swap positions here.
TeamSwap:InsertPosition( { x = -6 , y = 2 , z = -92 } );
TeamSwap:InsertPosition( { x = -6 , y = 3 , z = -92 } );
TeamSwap:InsertPosition( { x = -5 , y = 2 , z = -92 } );
TeamSwap:InsertPosition( { x = -5 , y = 3 , z = -92 } );


-- Current swap team status textboard.
local teamBoard = Game.EntityBlock.Create( { x = -7, y = 3, z = -92 } );
function SetTextBoard( text )
    if teamBoard ~= nil
    then
        teamBoard:Event( { action = "text" , value = text } , 0 );
    end
end
function ChangeTeamStatusText()
    if TeamSwap:GetNextTeam() == Game.TEAM.TR
    then
        SetTextBoard( "TR" );
    else
        SetTextBoard( "CT" );
    end
end


local isDoDelayedSwap = true;
local currentGameTime = 0;
local nextSwapTime = 0;


-- Team swap button.
local button = Game.EntityBlock.Create( { x = -7, y = 2, z = -93 } );
if button ~= nil
then
    function button:OnUse( player )
        -- Sice this hook is pre-executed BEFORE the button is running its childs,
        -- we need to delay it a bit.
        nextSwapTime = currentGameTime + 0.3;
        isDoDelayedSwap = true;

        ChangeTeamStatusText();
    end
end


function Game.Rule:OnUpdate( time )
    currentGameTime = time;

    if isDoDelayedSwap and currentGameTime >= nextSwapTime
    then
        isDoDelayedSwap = false;
        TeamSwap:DoSwap();
    end
end


-- Calls it once.
ChangeTeamStatusText();

